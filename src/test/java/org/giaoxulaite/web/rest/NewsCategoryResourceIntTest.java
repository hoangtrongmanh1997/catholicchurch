package org.giaoxulaite.web.rest;

import org.giaoxulaite.ChurchcatholicApp;

import org.giaoxulaite.domain.NewsCategory;
import org.giaoxulaite.repository.NewsCategoryRepository;
import org.giaoxulaite.service.NewsCategoryService;
import org.giaoxulaite.service.dto.NewsCategoryDTO;
import org.giaoxulaite.service.mapper.NewsCategoryMapper;
import org.giaoxulaite.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.giaoxulaite.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsCategoryResource REST controller.
 *
 * @see NewsCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChurchcatholicApp.class)
public class NewsCategoryResourceIntTest {

    private static final String DEFAULT_CATEGORY_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CATEGORY_DESC = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY_DESC = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private NewsCategoryRepository newsCategoryRepository;

    @Autowired
    private NewsCategoryMapper newsCategoryMapper;

    @Autowired
    private NewsCategoryService newsCategoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsCategoryMockMvc;

    private NewsCategory newsCategory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsCategoryResource newsCategoryResource = new NewsCategoryResource(newsCategoryService);
        this.restNewsCategoryMockMvc = MockMvcBuilders.standaloneSetup(newsCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsCategory createEntity(EntityManager em) {
        NewsCategory newsCategory = new NewsCategory()
            .categoryTitle(DEFAULT_CATEGORY_TITLE)
            .categoryDesc(DEFAULT_CATEGORY_DESC)
            .createdDate(DEFAULT_CREATED_DATE);
        return newsCategory;
    }

    @Before
    public void initTest() {
        newsCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsCategory() throws Exception {
        int databaseSizeBeforeCreate = newsCategoryRepository.findAll().size();

        // Create the NewsCategory
        NewsCategoryDTO newsCategoryDTO = newsCategoryMapper.toDto(newsCategory);
        restNewsCategoryMockMvc.perform(post("/api/news-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the NewsCategory in the database
        List<NewsCategory> newsCategoryList = newsCategoryRepository.findAll();
        assertThat(newsCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        NewsCategory testNewsCategory = newsCategoryList.get(newsCategoryList.size() - 1);
        assertThat(testNewsCategory.getCategoryTitle()).isEqualTo(DEFAULT_CATEGORY_TITLE);
        assertThat(testNewsCategory.getCategoryDesc()).isEqualTo(DEFAULT_CATEGORY_DESC);
        assertThat(testNewsCategory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
    }

    @Test
    @Transactional
    public void createNewsCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsCategoryRepository.findAll().size();

        // Create the NewsCategory with an existing ID
        newsCategory.setId(1L);
        NewsCategoryDTO newsCategoryDTO = newsCategoryMapper.toDto(newsCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsCategoryMockMvc.perform(post("/api/news-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NewsCategory in the database
        List<NewsCategory> newsCategoryList = newsCategoryRepository.findAll();
        assertThat(newsCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsCategories() throws Exception {
        // Initialize the database
        newsCategoryRepository.saveAndFlush(newsCategory);

        // Get all the newsCategoryList
        restNewsCategoryMockMvc.perform(get("/api/news-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryTitle").value(hasItem(DEFAULT_CATEGORY_TITLE.toString())))
            .andExpect(jsonPath("$.[*].categoryDesc").value(hasItem(DEFAULT_CATEGORY_DESC.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getNewsCategory() throws Exception {
        // Initialize the database
        newsCategoryRepository.saveAndFlush(newsCategory);

        // Get the newsCategory
        restNewsCategoryMockMvc.perform(get("/api/news-categories/{id}", newsCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsCategory.getId().intValue()))
            .andExpect(jsonPath("$.categoryTitle").value(DEFAULT_CATEGORY_TITLE.toString()))
            .andExpect(jsonPath("$.categoryDesc").value(DEFAULT_CATEGORY_DESC.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNewsCategory() throws Exception {
        // Get the newsCategory
        restNewsCategoryMockMvc.perform(get("/api/news-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsCategory() throws Exception {
        // Initialize the database
        newsCategoryRepository.saveAndFlush(newsCategory);
        int databaseSizeBeforeUpdate = newsCategoryRepository.findAll().size();

        // Update the newsCategory
        NewsCategory updatedNewsCategory = newsCategoryRepository.findOne(newsCategory.getId());
        // Disconnect from session so that the updates on updatedNewsCategory are not directly saved in db
        em.detach(updatedNewsCategory);
        updatedNewsCategory
            .categoryTitle(UPDATED_CATEGORY_TITLE)
            .categoryDesc(UPDATED_CATEGORY_DESC)
            .createdDate(UPDATED_CREATED_DATE);
        NewsCategoryDTO newsCategoryDTO = newsCategoryMapper.toDto(updatedNewsCategory);

        restNewsCategoryMockMvc.perform(put("/api/news-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the NewsCategory in the database
        List<NewsCategory> newsCategoryList = newsCategoryRepository.findAll();
        assertThat(newsCategoryList).hasSize(databaseSizeBeforeUpdate);
        NewsCategory testNewsCategory = newsCategoryList.get(newsCategoryList.size() - 1);
        assertThat(testNewsCategory.getCategoryTitle()).isEqualTo(UPDATED_CATEGORY_TITLE);
        assertThat(testNewsCategory.getCategoryDesc()).isEqualTo(UPDATED_CATEGORY_DESC);
        assertThat(testNewsCategory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsCategory() throws Exception {
        int databaseSizeBeforeUpdate = newsCategoryRepository.findAll().size();

        // Create the NewsCategory
        NewsCategoryDTO newsCategoryDTO = newsCategoryMapper.toDto(newsCategory);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsCategoryMockMvc.perform(put("/api/news-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the NewsCategory in the database
        List<NewsCategory> newsCategoryList = newsCategoryRepository.findAll();
        assertThat(newsCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsCategory() throws Exception {
        // Initialize the database
        newsCategoryRepository.saveAndFlush(newsCategory);
        int databaseSizeBeforeDelete = newsCategoryRepository.findAll().size();

        // Get the newsCategory
        restNewsCategoryMockMvc.perform(delete("/api/news-categories/{id}", newsCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<NewsCategory> newsCategoryList = newsCategoryRepository.findAll();
        assertThat(newsCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsCategory.class);
        NewsCategory newsCategory1 = new NewsCategory();
        newsCategory1.setId(1L);
        NewsCategory newsCategory2 = new NewsCategory();
        newsCategory2.setId(newsCategory1.getId());
        assertThat(newsCategory1).isEqualTo(newsCategory2);
        newsCategory2.setId(2L);
        assertThat(newsCategory1).isNotEqualTo(newsCategory2);
        newsCategory1.setId(null);
        assertThat(newsCategory1).isNotEqualTo(newsCategory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsCategoryDTO.class);
        NewsCategoryDTO newsCategoryDTO1 = new NewsCategoryDTO();
        newsCategoryDTO1.setId(1L);
        NewsCategoryDTO newsCategoryDTO2 = new NewsCategoryDTO();
        assertThat(newsCategoryDTO1).isNotEqualTo(newsCategoryDTO2);
        newsCategoryDTO2.setId(newsCategoryDTO1.getId());
        assertThat(newsCategoryDTO1).isEqualTo(newsCategoryDTO2);
        newsCategoryDTO2.setId(2L);
        assertThat(newsCategoryDTO1).isNotEqualTo(newsCategoryDTO2);
        newsCategoryDTO1.setId(null);
        assertThat(newsCategoryDTO1).isNotEqualTo(newsCategoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(newsCategoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(newsCategoryMapper.fromId(null)).isNull();
    }
}
