package org.giaoxulaite.web.rest;

import org.giaoxulaite.ChurchcatholicApp;

import org.giaoxulaite.domain.IFrame;
import org.giaoxulaite.repository.IFrameRepository;
import org.giaoxulaite.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.giaoxulaite.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.giaoxulaite.domain.enumeration.CateIFrame;
/**
 * Test class for the IFrameResource REST controller.
 *
 * @see IFrameResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChurchcatholicApp.class)
public class IFrameResourceIntTest {

    private static final String DEFAULT_LINK_IFRAME = "AAAAAAAAAA";
    private static final String UPDATED_LINK_IFRAME = "BBBBBBBBBB";

    private static final CateIFrame DEFAULT_CATE_IFRAME = CateIFrame.COVER;
    private static final CateIFrame UPDATED_CATE_IFRAME = CateIFrame.CUDIEU;

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    @Autowired
    private IFrameRepository iFrameRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restIFrameMockMvc;

    private IFrame iFrame;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IFrameResource iFrameResource = new IFrameResource(iFrameRepository);
        this.restIFrameMockMvc = MockMvcBuilders.standaloneSetup(iFrameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IFrame createEntity(EntityManager em) {
        IFrame iFrame = new IFrame()
            .linkIframe(DEFAULT_LINK_IFRAME)
            .cateIframe(DEFAULT_CATE_IFRAME)
            .updateDate(DEFAULT_UPDATE_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return iFrame;
    }

    @Before
    public void initTest() {
        iFrame = createEntity(em);
    }

    @Test
    @Transactional
    public void createIFrame() throws Exception {
        int databaseSizeBeforeCreate = iFrameRepository.findAll().size();

        // Create the IFrame
        restIFrameMockMvc.perform(post("/api/i-frames")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iFrame)))
            .andExpect(status().isCreated());

        // Validate the IFrame in the database
        List<IFrame> iFrameList = iFrameRepository.findAll();
        assertThat(iFrameList).hasSize(databaseSizeBeforeCreate + 1);
        IFrame testIFrame = iFrameList.get(iFrameList.size() - 1);
        assertThat(testIFrame.getLinkIframe()).isEqualTo(DEFAULT_LINK_IFRAME);
        assertThat(testIFrame.getCateIframe()).isEqualTo(DEFAULT_CATE_IFRAME);
        assertThat(testIFrame.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testIFrame.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    public void createIFrameWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = iFrameRepository.findAll().size();

        // Create the IFrame with an existing ID
        iFrame.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIFrameMockMvc.perform(post("/api/i-frames")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iFrame)))
            .andExpect(status().isBadRequest());

        // Validate the IFrame in the database
        List<IFrame> iFrameList = iFrameRepository.findAll();
        assertThat(iFrameList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllIFrames() throws Exception {
        // Initialize the database
        iFrameRepository.saveAndFlush(iFrame);

        // Get all the iFrameList
        restIFrameMockMvc.perform(get("/api/i-frames?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(iFrame.getId().intValue())))
            .andExpect(jsonPath("$.[*].linkIframe").value(hasItem(DEFAULT_LINK_IFRAME.toString())))
            .andExpect(jsonPath("$.[*].cateIframe").value(hasItem(DEFAULT_CATE_IFRAME.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY.toString())));
    }

    @Test
    @Transactional
    public void getIFrame() throws Exception {
        // Initialize the database
        iFrameRepository.saveAndFlush(iFrame);

        // Get the iFrame
        restIFrameMockMvc.perform(get("/api/i-frames/{id}", iFrame.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(iFrame.getId().intValue()))
            .andExpect(jsonPath("$.linkIframe").value(DEFAULT_LINK_IFRAME.toString()))
            .andExpect(jsonPath("$.cateIframe").value(DEFAULT_CATE_IFRAME.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingIFrame() throws Exception {
        // Get the iFrame
        restIFrameMockMvc.perform(get("/api/i-frames/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIFrame() throws Exception {
        // Initialize the database
        iFrameRepository.saveAndFlush(iFrame);
        int databaseSizeBeforeUpdate = iFrameRepository.findAll().size();

        // Update the iFrame
        IFrame updatedIFrame = iFrameRepository.findOne(iFrame.getId());
        // Disconnect from session so that the updates on updatedIFrame are not directly saved in db
        em.detach(updatedIFrame);
        updatedIFrame
            .linkIframe(UPDATED_LINK_IFRAME)
            .cateIframe(UPDATED_CATE_IFRAME)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restIFrameMockMvc.perform(put("/api/i-frames")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIFrame)))
            .andExpect(status().isOk());

        // Validate the IFrame in the database
        List<IFrame> iFrameList = iFrameRepository.findAll();
        assertThat(iFrameList).hasSize(databaseSizeBeforeUpdate);
        IFrame testIFrame = iFrameList.get(iFrameList.size() - 1);
        assertThat(testIFrame.getLinkIframe()).isEqualTo(UPDATED_LINK_IFRAME);
        assertThat(testIFrame.getCateIframe()).isEqualTo(UPDATED_CATE_IFRAME);
        assertThat(testIFrame.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testIFrame.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingIFrame() throws Exception {
        int databaseSizeBeforeUpdate = iFrameRepository.findAll().size();

        // Create the IFrame

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restIFrameMockMvc.perform(put("/api/i-frames")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iFrame)))
            .andExpect(status().isCreated());

        // Validate the IFrame in the database
        List<IFrame> iFrameList = iFrameRepository.findAll();
        assertThat(iFrameList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteIFrame() throws Exception {
        // Initialize the database
        iFrameRepository.saveAndFlush(iFrame);
        int databaseSizeBeforeDelete = iFrameRepository.findAll().size();

        // Get the iFrame
        restIFrameMockMvc.perform(delete("/api/i-frames/{id}", iFrame.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<IFrame> iFrameList = iFrameRepository.findAll();
        assertThat(iFrameList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IFrame.class);
        IFrame iFrame1 = new IFrame();
        iFrame1.setId(1L);
        IFrame iFrame2 = new IFrame();
        iFrame2.setId(iFrame1.getId());
        assertThat(iFrame1).isEqualTo(iFrame2);
        iFrame2.setId(2L);
        assertThat(iFrame1).isNotEqualTo(iFrame2);
        iFrame1.setId(null);
        assertThat(iFrame1).isNotEqualTo(iFrame2);
    }
}
