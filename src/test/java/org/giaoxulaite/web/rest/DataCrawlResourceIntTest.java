package org.giaoxulaite.web.rest;

import org.giaoxulaite.ChurchcatholicApp;

import org.giaoxulaite.domain.DataCrawl;
import org.giaoxulaite.repository.DataCrawlRepository;
import org.giaoxulaite.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.giaoxulaite.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataCrawlResource REST controller.
 *
 * @see DataCrawlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChurchcatholicApp.class)
public class DataCrawlResourceIntTest {

    private static final String DEFAULT_LINK_IMG_BG = "AAAAAAAAAA";
    private static final String UPDATED_LINK_IMG_BG = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE_POST = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_POST = "BBBBBBBBBB";

    private static final String DEFAULT_DESC_POST = "AAAAAAAAAA";
    private static final String UPDATED_DESC_POST = "BBBBBBBBBB";

    private static final String DEFAULT_TIME_POST = "AAAAAAAAAA";
    private static final String UPDATED_TIME_POST = "BBBBBBBBBB";

    private static final String DEFAULT_LINK_POST = "AAAAAAAAAA";
    private static final String UPDATED_LINK_POST = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT_POST = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT_POST = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    @Autowired
    private DataCrawlRepository dataCrawlRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataCrawlMockMvc;

    private DataCrawl dataCrawl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataCrawlResource dataCrawlResource = new DataCrawlResource(dataCrawlRepository);
        this.restDataCrawlMockMvc = MockMvcBuilders.standaloneSetup(dataCrawlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataCrawl createEntity(EntityManager em) {
        DataCrawl dataCrawl = new DataCrawl()
            .linkImgBg(DEFAULT_LINK_IMG_BG)
            .titlePost(DEFAULT_TITLE_POST)
            .descPost(DEFAULT_DESC_POST)
            .timePost(DEFAULT_TIME_POST)
            .linkPost(DEFAULT_LINK_POST)
            .contentPost(DEFAULT_CONTENT_POST)
            .source(DEFAULT_SOURCE);
        return dataCrawl;
    }

    @Before
    public void initTest() {
        dataCrawl = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataCrawl() throws Exception {
        int databaseSizeBeforeCreate = dataCrawlRepository.findAll().size();

        // Create the DataCrawl
        restDataCrawlMockMvc.perform(post("/api/data-crawls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCrawl)))
            .andExpect(status().isCreated());

        // Validate the DataCrawl in the database
        List<DataCrawl> dataCrawlList = dataCrawlRepository.findAll();
        assertThat(dataCrawlList).hasSize(databaseSizeBeforeCreate + 1);
        DataCrawl testDataCrawl = dataCrawlList.get(dataCrawlList.size() - 1);
        assertThat(testDataCrawl.getLinkImgBg()).isEqualTo(DEFAULT_LINK_IMG_BG);
        assertThat(testDataCrawl.getTitlePost()).isEqualTo(DEFAULT_TITLE_POST);
        assertThat(testDataCrawl.getDescPost()).isEqualTo(DEFAULT_DESC_POST);
        assertThat(testDataCrawl.getTimePost()).isEqualTo(DEFAULT_TIME_POST);
        assertThat(testDataCrawl.getLinkPost()).isEqualTo(DEFAULT_LINK_POST);
        assertThat(testDataCrawl.getContentPost()).isEqualTo(DEFAULT_CONTENT_POST);
        assertThat(testDataCrawl.getSource()).isEqualTo(DEFAULT_SOURCE);
    }

    @Test
    @Transactional
    public void createDataCrawlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataCrawlRepository.findAll().size();

        // Create the DataCrawl with an existing ID
        dataCrawl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataCrawlMockMvc.perform(post("/api/data-crawls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCrawl)))
            .andExpect(status().isBadRequest());

        // Validate the DataCrawl in the database
        List<DataCrawl> dataCrawlList = dataCrawlRepository.findAll();
        assertThat(dataCrawlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDataCrawls() throws Exception {
        // Initialize the database
        dataCrawlRepository.saveAndFlush(dataCrawl);

        // Get all the dataCrawlList
        restDataCrawlMockMvc.perform(get("/api/data-crawls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataCrawl.getId().intValue())))
            .andExpect(jsonPath("$.[*].linkImgBg").value(hasItem(DEFAULT_LINK_IMG_BG.toString())))
            .andExpect(jsonPath("$.[*].titlePost").value(hasItem(DEFAULT_TITLE_POST.toString())))
            .andExpect(jsonPath("$.[*].descPost").value(hasItem(DEFAULT_DESC_POST.toString())))
            .andExpect(jsonPath("$.[*].timePost").value(hasItem(DEFAULT_TIME_POST.toString())))
            .andExpect(jsonPath("$.[*].linkPost").value(hasItem(DEFAULT_LINK_POST.toString())))
            .andExpect(jsonPath("$.[*].contentPost").value(hasItem(DEFAULT_CONTENT_POST.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())));
    }

    @Test
    @Transactional
    public void getDataCrawl() throws Exception {
        // Initialize the database
        dataCrawlRepository.saveAndFlush(dataCrawl);

        // Get the dataCrawl
        restDataCrawlMockMvc.perform(get("/api/data-crawls/{id}", dataCrawl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataCrawl.getId().intValue()))
            .andExpect(jsonPath("$.linkImgBg").value(DEFAULT_LINK_IMG_BG.toString()))
            .andExpect(jsonPath("$.titlePost").value(DEFAULT_TITLE_POST.toString()))
            .andExpect(jsonPath("$.descPost").value(DEFAULT_DESC_POST.toString()))
            .andExpect(jsonPath("$.timePost").value(DEFAULT_TIME_POST.toString()))
            .andExpect(jsonPath("$.linkPost").value(DEFAULT_LINK_POST.toString()))
            .andExpect(jsonPath("$.contentPost").value(DEFAULT_CONTENT_POST.toString()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataCrawl() throws Exception {
        // Get the dataCrawl
        restDataCrawlMockMvc.perform(get("/api/data-crawls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataCrawl() throws Exception {
        // Initialize the database
        dataCrawlRepository.saveAndFlush(dataCrawl);
        int databaseSizeBeforeUpdate = dataCrawlRepository.findAll().size();

        // Update the dataCrawl
        DataCrawl updatedDataCrawl = dataCrawlRepository.findOne(dataCrawl.getId());
        // Disconnect from session so that the updates on updatedDataCrawl are not directly saved in db
        em.detach(updatedDataCrawl);
        updatedDataCrawl
            .linkImgBg(UPDATED_LINK_IMG_BG)
            .titlePost(UPDATED_TITLE_POST)
            .descPost(UPDATED_DESC_POST)
            .timePost(UPDATED_TIME_POST)
            .linkPost(UPDATED_LINK_POST)
            .contentPost(UPDATED_CONTENT_POST)
            .source(UPDATED_SOURCE);

        restDataCrawlMockMvc.perform(put("/api/data-crawls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataCrawl)))
            .andExpect(status().isOk());

        // Validate the DataCrawl in the database
        List<DataCrawl> dataCrawlList = dataCrawlRepository.findAll();
        assertThat(dataCrawlList).hasSize(databaseSizeBeforeUpdate);
        DataCrawl testDataCrawl = dataCrawlList.get(dataCrawlList.size() - 1);
        assertThat(testDataCrawl.getLinkImgBg()).isEqualTo(UPDATED_LINK_IMG_BG);
        assertThat(testDataCrawl.getTitlePost()).isEqualTo(UPDATED_TITLE_POST);
        assertThat(testDataCrawl.getDescPost()).isEqualTo(UPDATED_DESC_POST);
        assertThat(testDataCrawl.getTimePost()).isEqualTo(UPDATED_TIME_POST);
        assertThat(testDataCrawl.getLinkPost()).isEqualTo(UPDATED_LINK_POST);
        assertThat(testDataCrawl.getContentPost()).isEqualTo(UPDATED_CONTENT_POST);
        assertThat(testDataCrawl.getSource()).isEqualTo(UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void updateNonExistingDataCrawl() throws Exception {
        int databaseSizeBeforeUpdate = dataCrawlRepository.findAll().size();

        // Create the DataCrawl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataCrawlMockMvc.perform(put("/api/data-crawls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCrawl)))
            .andExpect(status().isCreated());

        // Validate the DataCrawl in the database
        List<DataCrawl> dataCrawlList = dataCrawlRepository.findAll();
        assertThat(dataCrawlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataCrawl() throws Exception {
        // Initialize the database
        dataCrawlRepository.saveAndFlush(dataCrawl);
        int databaseSizeBeforeDelete = dataCrawlRepository.findAll().size();

        // Get the dataCrawl
        restDataCrawlMockMvc.perform(delete("/api/data-crawls/{id}", dataCrawl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DataCrawl> dataCrawlList = dataCrawlRepository.findAll();
        assertThat(dataCrawlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataCrawl.class);
        DataCrawl dataCrawl1 = new DataCrawl();
        dataCrawl1.setId(1L);
        DataCrawl dataCrawl2 = new DataCrawl();
        dataCrawl2.setId(dataCrawl1.getId());
        assertThat(dataCrawl1).isEqualTo(dataCrawl2);
        dataCrawl2.setId(2L);
        assertThat(dataCrawl1).isNotEqualTo(dataCrawl2);
        dataCrawl1.setId(null);
        assertThat(dataCrawl1).isNotEqualTo(dataCrawl2);
    }
}
