(function () {
    'use strict';

    angular
        .module('churchcatholicApp')
        .factory('GetAllQuestionDisplay', GetAllQuestionDisplay)
        .factory('GetListStatusAnswered', GetListStatusAnswered);

    GetAllQuestionDisplay.$inject = ['$resource', 'DateUtils'];
    GetListStatusAnswered.$inject = ['$resource'];

    function GetAllQuestionDisplay ($resource, DateUtils) {
        var service = $resource('/api/question/listQuestionsDisplay', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    return data;
                }
            }
        });

        return service;
    }

    function GetListStatusAnswered($resource) {
        var service = $resource('/api/list-status-answered', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
        return service;
    }
})();
