(function() {
    'use strict';
    angular
        .module('churchcatholicApp')
        .factory('Question', Question)
        .factory('Answer', Answer)
        .factory('GetFullQuesAndAns', GetFullQuesAndAns)
        .factory('DeleteQuestionContainAnswer', DeleteQuestionContainAnswer)
        .factory('GetQuesAndAns', GetQuesAndAns);

    Question.$inject = ['$resource', 'DateUtils'];
    Answer.$inject = ['$resource'];
    GetFullQuesAndAns.$inject = ['$resource', 'DateUtils'];
    GetQuesAndAns.$inject = ['$resource', 'DateUtils'];
    DeleteQuestionContainAnswer.$inject = ['$resource']

    function Question($resource, DateUtils) {
        var resourceUrl = 'api/questions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }

    function Answer($resource) {
        var resourceUrl = 'api/answers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }

    function GetFullQuesAndAns($resource, DateUtils) {
        var resourceUrl = "api/get-full-question-answer";

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            }
        })
    }

    function GetQuesAndAns($resource, DateUtils) {
        var resourceUrl = "api/get-question-answer/:id";

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isObject: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            }
        })
    }

    function DeleteQuestionContainAnswer($resource) {
        var resourceUrl = "api/questions/delete/:id";

        return $resource(resourceUrl, {}, {
            'remove': { method:'DELETE' }
        })
    }
})();
