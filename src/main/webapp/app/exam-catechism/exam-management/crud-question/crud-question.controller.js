(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('CrudQuestionController', CrudQuestionController)

    CrudQuestionController.$inject = ['Answer', 'Question', 'GetFullQuesAndAns', '$scope', '$compile', '$uibModalInstance', 'entity', '$timeout', '$http', 'DeleteQuestionContainAnswer'];

    function CrudQuestionController(Answer, Question, GetFullQuesAndAns, $scope, $compile, $uibModalInstance, entity, $timeout, $http, DeleteQuestionContainAnswer) {

        var vm = this;
        vm.question = entity;
        console.log(entity)
        vm.clear = clear;
        vm.save = save;
        vm.countAnswer = [];
        vm.confirmDelete = confirmDelete;

        if (!vm.question.id) {
            vm.answer = [];
            vm.countClick = 0;
        } else {
            if (vm.question.answer) {
                vm.answer = vm.question.answer;
                vm.countClick = vm.answer.length;
                for (let i = 0; i < vm.countClick; ++i) {
                    vm.countAnswer.push(1);
                }
            } else {
                vm.answer = [];
                vm.countClick = 0;
            }
        }

        vm.addFormCreateAns = addFormCreateAns;

        function addFormCreateAns() {
            vm.countClick += 1;
            // let formCreateAns = 
            // 	'<div class="form-group">' +
            //     	'<label class="control-label" data-translate="churchcatholicApp.answer.answerContent" translate-values="{orderAns: {{vm.countClick}}}" for="field_answerContent">Answer Content</label>'
            //     	+   '<input type="text" class="form-control" name="answerContent" id="field_answerContent" ng-model="vm.answer.answerContent" />'
            //     +'</div>' + 
            //         '<div class="form-group">' +
            //             '<label class="control-label" data-translate="churchcatholicApp.answer.status" for="field_status">Status</label>'
            //         +   '<input type="checkbox" name="status" id="field_status" ng-model="vm.answer.status" />'
            //         + '</div>' + '<hr/>';
            // angular.element(document.getElementById('form-create-question')).append($compile(formCreateAns)($scope));
            if (vm.countClick > 0) {
                vm.countAnswer.push(1);
            }
        }

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.question.id !== null) {
                Question.update(vm.question);
                if (vm.answer.length > 0) {
                    vm.answer.forEach(function(item) {
                        if (item.id !== null) {
                            item.questionId = vm.question.id;
                            Answer.update(item, onSaveSuccess, onSaveError);
                        } else {
                            item.questionId = vm.question.id;
                            Answer.save(item, onSaveSuccess, onSaveError);
                        }
                    })
                }
            } else {
                vm.question.createdDate = new Date();
                $http({
                    method: 'POST',
                    url: '/api/questions',
                    data: vm.question
                }).then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log(response)
                    vm.answer.forEach(function(item) {
                        item.questionId = response.data.id;
                        Answer.save(item, onSaveSuccess, onSaveError);
                    })
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('churchcatholicApp:questionAnswerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function confirmDelete(idQuestion) {
        	DeleteQuestionContainAnswer.remove({id: idQuestion},
        		function () {
                    $uibModalInstance.close(true);
                });
        }

    }
})();
