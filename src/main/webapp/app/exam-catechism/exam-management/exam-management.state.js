(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .config(stateConfig)

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('exam-management', {
            	parent: 'admin',
            	url: '/quan-ly-bai-thi',
            	data: {
		            authorities: ['ROLE_MANAGER', 'ROLE_ADMIN'],
		            pageTitle: ''
		        },
		        views: {
		            'content@': {
		                templateUrl: 'app/exam-catechism/exam-management/exam-management.html',
		                controller: 'ExamManagementController',
		                controllerAs: 'vm'
		            }
		        }, 
		        resolve: {
	                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
	                    $translatePartialLoader.addPart('answer');
	                    $translatePartialLoader.addPart('question');
	                    $translatePartialLoader.addPart('global');
	                    $translatePartialLoader.addPart('level');
	                    return $translate.refresh();
	                }]
	            }
            })

            .state('create-new-question', {
            	parent: 'exam-management',
            	url: '/them-cau-hoi',
            	data: {
		            authorities: ['ROLE_MANAGER', 'ROLE_ADMIN'],
		            pageTitle: ''
		        },
		        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
	                $uibModal.open({
	                    templateUrl: 'app/exam-catechism/exam-management/crud-question/create-question.html',
		                controller: 'CrudQuestionController',
		                controllerAs: 'vm',
	                    backdrop: 'static',
	                    size: 'lg',
	                    resolve: {
	                        entity: function () {
	                            return {
	                                questionTitle: null,
	                                source: null,
	                                createdDate: null,
	                                level: null,
	                                id: null
	                            };
	                        }
	                    }
	                }).result.then(function() {
	                    $state.go('exam-management', null, { reload: true });
	                }, function() {
	                    $state.go('exam-management');
	                });
	            }]
            })

            .state('update-question', {
            	parent: 'exam-management',
            	url: '/sua-cau-hoi-{id}',
            	data: {
		            authorities: ['ROLE_MANAGER', 'ROLE_ADMIN'],
		            pageTitle: ''
		        },
		        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
	                $uibModal.open({
	                    templateUrl: 'app/exam-catechism/exam-management/crud-question/create-question.html',
		                controller: 'CrudQuestionController',
		                controllerAs: 'vm',
	                    backdrop: 'static',
	                    size: 'lg',
	                    resolve: {
	                        entity: ['GetQuesAndAns', function(GetQuesAndAns) {
	                            return GetQuesAndAns.get({id : $stateParams.id}).$promise;
	                        }]
	                    }
	                }).result.then(function() {
	                    $state.go('exam-management', null, { reload: true });
	                }, function() {
	                    $state.go('exam-management');
	                });
	            }]
            })

            .state('delete-question', {
            	parent: 'exam-management',
            	url: '/xoa-cau-hoi-{id}',
            	data: {
		            authorities: ['ROLE_MANAGER', 'ROLE_ADMIN'],
		            pageTitle: ''
		        },
		        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
	                $uibModal.open({
	                    templateUrl: 'app/exam-catechism/exam-management/crud-question/confirm-remove-question.html',
		                controller: 'CrudQuestionController',
		                controllerAs: 'vm',
	                    backdrop: 'static',
	                    size: 'lg',
	                    resolve: {
	                        entity: ['Question', function(Question) {
	                            return Question.get({id : $stateParams.id}).$promise;
	                        }]
	                    }
	                }).result.then(function() {
	                    $state.go('exam-management', null, { reload: true });
	                }, function() {
	                    $state.go('exam-management');
	                });
	            }]
            })
    }

})();
