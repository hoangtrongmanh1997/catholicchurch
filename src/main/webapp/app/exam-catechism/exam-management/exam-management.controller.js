(function() {
	'use strict';

	angular
		.module('churchcatholicApp')
		.controller('ExamManagementController', ExamManagementController)

	ExamManagementController.$inject = ['GetFullQuesAndAns'];

	function ExamManagementController(GetFullQuesAndAns) {

		var vm = this;
		vm.page = 0;
		const sizeOnePage = 20;
		vm.flag = true;

		getFullQuesAndAns(vm.page);

		function getFullQuesAndAns(pageNumber) {
			GetFullQuesAndAns.query({page: pageNumber, size: sizeOnePage}, function(res) {
				if(res.length) {
					res.forEach(function(item) {
						item.sizeAnswer = item.answer.length;
					})
					vm.listFullQuesAndAns = res;
					vm.flag = true;
				} else {
					vm.flag = false;
				}

			})
		}

		vm.prevPage = prevPage;

		function prevPage() {
			vm.page -= 1;
			getFullQuesAndAns(vm.page);
		}

		vm.nextPage = nextPage;
		function nextPage() {
			vm.page += 1;
			getFullQuesAndAns(vm.page);
		}

	}
})();