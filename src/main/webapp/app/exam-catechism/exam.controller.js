(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('ExamController', ExamController);

    ExamController.$inject = ['$scope', '$sce', 'Principal', '$state', 'pagingParams', 'GetAllQuestionDisplay', 'GetListStatusAnswered', '$mdDialog', '$q', '$location', '$anchorScroll'];

    function ExamController($scope, $sce, Principal, $state, pagingParams, GetAllQuestionDisplay, GetListStatusAnswered, $mdDialog, $q, $location, $anchorScroll) {
        var vm = this;
        vm.statusAnswered = [];
        vm.listOrderQuestion = [];
        GetAllQuestionDisplay.query({}, function(res) {
            for (var i = 0; i < res.length; ++i) {
                res[i].order = i + 1;
                vm.listOrderQuestion[i] = i + 1;
            }
            // console.log(res)
            vm.listQuestionsDisplay = res;
        });

        function checkResultTest() {
            var deferred = $q.defer();
            GetListStatusAnswered.query({ listId: vm.statusAnswered }, function(res) {
                var countAnsTrue = 0;
                if (res) {
                    res.forEach(function(itemAnsInfo) {
                        if (itemAnsInfo.status == true) {
                            countAnsTrue++;
                        }
                        // console.log(countAnsTrue)
                    });
                }
                vm.countAnsTrue = countAnsTrue;
            })
            return $q(function(resolve, reject) {
                setTimeout(function() {
                    console.log(vm.countAnsTrue)
                    resolve(vm.countAnsTrue);
                }, 500);
            });
        }

        vm.popDialogResultTest = popDialogResultTest;

        function popDialogResultTest() {
            var resultTest;
            checkResultTest().then(function(res) {
                resultTest = res;
                console.log(res)
                $mdDialog.show({
                    template: '<div style="padding: 10px"><span>Số câu trả lời đúng: </span>' +
                        '<span>' + " " + resultTest + '</span>' +
                        '</div>',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: true,
                }).then(function(result) {

                }, function() {

                });
            });
        }

        vm.scrollToQuestion = function(id) {
            $location.hash(id);
            console.log($location.hash());
            $anchorScroll();
        };
    }
})();
