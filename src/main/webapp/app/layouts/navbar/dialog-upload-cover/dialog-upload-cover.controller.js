(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('DialogUploadCoverCtrl', DialogUploadCoverCtrl);

    DialogUploadCoverCtrl.$inject = ['$state', 'entity', 'Principal', '$timeout', 'IFrame', '$scope', '$mdDialog', '$http'];

    function DialogUploadCoverCtrl($state, entity, Principal, $timeout, IFrame, $scope, $mdDialog, $http) {
        var vm = this;

        vm.iFrame = entity;
        vm.save = save;
        vm.clear = clear;

        var loginUser;
        Principal.identity().then(function(account) {
            // console.log(account);
            loginUser = account.lastName + " " + account.firstName;
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $mdDialog.cancel();
        }

        function save () {
            vm.iFrame.updateDate = new Date();
            vm.iFrame.updateBy = loginUser;
            vm.isSaving = true;
            IFrame.save(vm.iFrame, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess (result) {
            $scope.$emit('churchcatholicApp:iFrameUpdate', result);
            $mdDialog.cancel();
            vm.isSaving = false;
            $state.go($state.current, null, { reload: true });
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        $scope.createLinkUpload = createLinkUpload;
        vm.isUpload = true;
        function createLinkUpload() {
            vm.isUpload = true;
            var formData = new FormData();
            formData.append('file', angular.element(document.querySelector('#uploadFileInput'))[0].files[0]);
            // PostFileAttach.upload(formData, onUploadSuccess, onUploadError);
            $http({
                method: 'POST',
                url: '/api/upload/upload-file',
                data: formData,
                //assign content-type as undefined, the browser
                //will assign the correct boundary for us
                headers: { 'Content-Type': undefined },
                //prevents serializing payload.  don't do it.
                transformRequest: angular.identity
                // 2 lines above help create link file upload
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                vm.iFrame.linkIframe = response.data.link;
                vm.isUpload = false;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response.status)
                vm.isUpload = false;
            });


        }
    }
})();
