(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', 'NewsCategory', '$mdDialog', 'IFrame', '$sce'];

    function NavbarController($state, Auth, Principal, ProfileService, LoginService, NewsCategory, $mdDialog, IFrame, $sce) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.isAuthenticated = Principal.isAuthenticated;

        ProfileService.getProfileInfo().then(function(response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;

        function login() {
            collapseNavbar();
            LoginService.open();
        }

        function logout() {
            collapseNavbar();
            Auth.logout();
            $state.go('home');
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }

        vm.listNewsCateHoiDoan = [];
        vm.listNewsCateOthers = [];
        NewsCategory.query(function(res) {
            vm.listNewsCate = res;
            if (vm.listNewsCate) {
                vm.listNewsCate.forEach(function(itemCate) {
                    // console.log(itemCate)
                    if (itemCate.typeCate == "HOIDOAN") {
                        vm.listNewsCateHoiDoan.push(itemCate);
                        // console.log(vm.listNewsCateHoiDoan)
                    } else if (itemCate.typeCate == "OTHERS") {
                        vm.listNewsCateOthers.push(itemCate);
                        // console.log(vm.listNewsCateOthers)
                    }
                })
            }
        });

        vm.uploadIframe = uploadIframe;

        function uploadIframe(cateIframe, type) {
            var linkTemplate = null;
            if (cateIframe == "COVER") {
                if(type == "local") {
                    linkTemplate = "app/layouts/navbar/dialog-upload-cover/dialog-upload-cover-pc.html";
                } else {
                    linkTemplate = "app/layouts/navbar/dialog-upload-cover/dialog-upload-cover-link.html";
                }
            } else {
                linkTemplate = "app/layouts/navbar/dialog-upload-cover/dialog-upload-cover-link.html";
            }
            // vm.flagPC = true;
            $mdDialog.show({
                controller: 'DialogUploadCoverCtrl',
                controllerAs: 'vm',
                templateUrl: linkTemplate,
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: true,
                resolve: {
                    entity: function() {
                        return {
                            linkIframe: null,
                            cateIframe: cateIframe,
                            updateDate: null,
                            updateBy: null
                        };
                    },
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('iFrame');
                        $translatePartialLoader.addPart('cateIFrame');
                        return $translate.refresh();
                    }]
                }
            }).then(function(result) {

            }, function() {

            });
        }

        // vm.uploadImgByLink = uploadImgByLink;

        // function uploadImgByLink() {
        //     // vm.flagPC = false;
        //     $mdDialog.show({
        //         controller: 'DialogUploadCoverCtrl',
        //         controllerAs: 'vm',
        //         templateUrl: 'app/layouts/navbar/dialog-upload-cover/dialog-upload-cover-link.html',
        //         parent: angular.element(document.body),
        //         clickOutsideToClose: false,
        //         fullscreen: true,
        //         resolve: {
        //             entity: function() {
        //                 return {
        //                     linkIframe: null,
        //                     cateIframe: null,
        //                     updateDate: null,
        //                     updateBy: null
        //                 };
        //             },
        //             translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
        //                 $translatePartialLoader.addPart('global');
        //                 $translatePartialLoader.addPart('iFrame');
        //                 $translatePartialLoader.addPart('cateIFrame');
        //                 return $translate.refresh();
        //             }]
        //         }
        //     }).then(function (result) {

        //     }, function () {

        //     });
        // }
        
        vm.trustHtml = trustHtml;

        function trustHtml(code) {
            return $sce.trustAsHtml(code);
        }

        vm.listIframeCover = [];
        vm.listIframeCuDieu = [];
        vm.listIframeIntroduce = [];
        IFrame.query(function(res) {
            vm.countIframeCover = 0;
            vm.countIframeIntro = 0;
            vm.countIframeDance = 0;
            res.forEach(function(item) {
                if (item.cateIframe == "COVER") {
                    vm.listIframeCover.push(item);
                    vm.countIframeCover++;
                } else if (item.cateIframe == "CUDIEU") {
                    vm.listIframeCuDieu.push(item);
                    vm.countIframeDance++;
                } else if (item.cateIframe == "INTRODUCE") {
                    vm.listIframeIntroduce.push(item);
                    vm.countIframeIntro++;
                }
            })
        })

    }
})();
