(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .directive('fixed', fixed);

    fixed.$inject = ['$window'];

    function fixed($window) {

        return {
            restrict: 'A',
            link: function(scope, element, attrs) {

                var topClass = attrs.fixed,
                    offsetTop = 190;

                angular.element($window).bind("scroll", function() {
                    if (this.pageYOffset >= offsetTop) {
                        angular.element('.scrollTop').css('opacity', '1');
                        // console.log(this.pageYOffset)
                        element.addClass(topClass);
                    } else {
                        element.removeClass(topClass);
                        angular.element('.scrollTop').css('opacity', '0');
                    }
                });
                //Click event to scroll to top
                angular.element('.scrollTop').click(function() {
                    angular.element('html, body').animate({
                        scrollTop: 0
                    }, 800);
                    return false;

                }); // click() scroll top EMD
            }
        };
    }
})();
