(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'News', 'pagingParams', 'AlertService', 'GetDataCrawlHandleCommon'];

    function HomeController($scope, Principal, LoginService, $state, News, pagingParams, AlertService, GetDataCrawlHandleCommon) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.listNewsGeneral = [];
        vm.flag = true;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function register() {
            $state.go('register');
        }

        News.query(function(res){
            vm.listNewsGeneralSize = res.length;
            console.log(vm.listNewsGeneralSize)
            vm.page = Math.ceil(vm.listNewsGeneralSize/7) - 1;
            console.log(res)
            News.query({
                page: Math.ceil(vm.listNewsGeneralSize/7) - 1,
                size: pagingParams.size
            }, onSuccess, onError);
        });

        // vm.page = pagingParams.page - 1;
        // News.query({
        //     page: pagingParams.page - 1,
        //     size: pagingParams.size
        // }, onSuccess, onError);

        vm.transition = transition;
        function transition() {
            News.query({
                page: vm.page,
                size: pagingParams.size
            }, onSuccess, onError);
        }

        function onSuccess(data, headers) {
            vm.page = vm.page - 1;
            console.log(vm.page)
            data.forEach(function(res){
                vm.listNewsGeneral.push(res);
            })
            if(data.length == 0 || vm.listNewsGeneral.length >= vm.listNewsGeneralSize) {
                vm.flag = false;
            } else {
                if(vm.listNewsGeneral.length <= 5) {
                    News.query({
                        page: vm.page,
                        size: pagingParams.size
                    }, onSuccess, onError); 
                }
            }
        }

        function onError(error) {
            AlertService.error(error.data.message);
        }
        vm.listData = GetDataCrawlHandleCommon.getListData();
        vm.listPostThaiHa2 = vm.listData.listPostThaiHa2;
        vm.listPostThaiHa3 = vm.listData.listPostThaiHa3;
        vm.listPostLctx = vm.listData.listPostLctx;        

    }
})();
