(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
