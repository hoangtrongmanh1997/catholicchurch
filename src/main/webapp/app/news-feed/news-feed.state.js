(function() {
    'use strict'

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('news-management', {
                parent: 'admin',
                url: '/quan-ly-tin-tuc',
                data: {
                    authorities: ['ROLE_ADMIN', 'ROLE_MANAGER'],
                    pageTitle: 'churchcatholicApp.news.management.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/news-feed/news-management/news-management-home.html',
                        controller: 'NewsManagementHomeController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('news');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('news-display', {
                parent: 'app',
                url: '/tin-tuc/{newsPath}-{id}',
                data: {
                    authorities: [],
                    pageTitle: 'churchcatholicApp.news.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/news-feed/news-display/news-display.html',
                        controller: 'NewsDisplayController',
                        controllerAs: 'vm'
                    }
                },
                // params: {
                //     id: null
                // },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('news');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'News', function($stateParams, News) {
                        return News.get({id : $stateParams.id}).$promise;
                    }]

                }
            })
    }

})();
