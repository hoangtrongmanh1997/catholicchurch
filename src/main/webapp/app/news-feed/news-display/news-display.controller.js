(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsDisplayController', NewsDisplayController);

    NewsDisplayController.$inject = ['$scope', '$state', 'NewsCategory', 'GetListNewsByCate', 'News', 'entity', '$sce'];

    function NewsDisplayController($scope, $state, NewsCategory, GetListNewsByCate, News, entity, $sce) {
        var vm = this;

        vm.newsDisplay = entity;
        // console.log(entity)
        vm.idNewsDisplay = vm.newsDisplay.id;

        vm.trustHtml = trustHtml;

        function trustHtml(code) {
            return $sce.trustAsHtml(code);
        }

        GetListNewsByCate.query({ id: entity.newsCategoryId }, function(res) {
            vm.listNewsRelate = [];
            res[0].newsListInCate.forEach(function(newsItem){
                if(newsItem.id != vm.idNewsDisplay) {
                    vm.listNewsRelate.push(newsItem)
                }
            });
        });
    }
})();
