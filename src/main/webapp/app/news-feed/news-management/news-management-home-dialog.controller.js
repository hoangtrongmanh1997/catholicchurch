(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsManagementHomeDialogController', NewsManagementHomeDialogController);

    NewsManagementHomeDialogController.$inject = ['$scope', '$state', 'Principal', '$mdDialog', 'entity', 'NewsCategory', 'News', '$http'];

    function NewsManagementHomeDialogController($scope, $state, Principal, $mdDialog, entity, NewsCategory, News, $http) {
        var vm = this;

        vm.contentPost = entity;
        vm.nameTypeUploagImg = "Upload IMAGE from your PC";

        vm.newscategories = NewsCategory.query();

        var quill;
        angular.element(document).ready(function() {
            var toolbarOptions = [
                ['bold', 'italic', 'underline', 'strike'], // toggled buttons
                ['blockquote', 'code-block'],

                [{ 'header': 1 }, { 'header': 2 }, { 'header': 3 }], // custom button values
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
                [{ 'indent': '-1' }, { 'indent': '+1' }], // outdent/indent
                [{ 'direction': 'rtl' }], // text direction

                [{ 'size': ['small', false, 'large', 'huge'] }], // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                ['link', 'image', 'video', 'formula'], // add's image support

                [{ 'color': [] }, { 'background': [] }], // dropdown with defaults from theme
                [{ 'font': [] }],
                [{ 'align': [] }],

                ['clean'] // remove formatting button
            ];

            quill = new Quill('#editor', {
                modules: {
                    toolbar: toolbarOptions
                },
                theme: 'snow'
            });

            function imageHandler() {
                var range = quill.getSelection();
                var value = prompt('What is the image URL');
                quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
            }

            var toolbar = quill.getModule('toolbar');
            vm.changeHandler = toolbar.handlers['image'];
            vm.changeTypeUploadImg = changeTypeUploadImg;

            function changeTypeUploadImg() {
                if (vm.typeUploadImg) {
                    vm.nameTypeUploagImg = "Upload IMAGE by URL";
                    toolbar.addHandler('image', imageHandler);
                    // console.log(vm.changeHandler)

                } else {
                    vm.nameTypeUploagImg = "Upload IMAGE from your PC";
                    toolbar.addHandler('image', vm.changeHandler);

                }
            }

            quill.root.innerHTML = vm.contentPost.content;

            console.log(vm.contentPost.content)

        });
        var loginUser;
        Principal.identity().then(function(account) {
            // console.log(account);
            loginUser = account.lastName + " " + account.firstName;
        });
        vm.save = function save() {
            vm.contentPost.content = quill.container.firstChild.innerHTML;
            vm.contentPost.createdBy = loginUser;
            vm.contentPost.newsPath = to_slug(vm.contentPost.newsTitle);
            vm.isSaving = true;
            if (vm.contentPost.id != null) {
                News.update(vm.contentPost, onSaveSuccess, onSaveError);
            } else {
                vm.contentPost.createdDate = new Date();
                News.save(vm.contentPost, onSaveSuccess, onSaveError);
            }
            // console.log(quill.container.firstChild.innerHTML);
        }

        function onSaveSuccess(result) {
            $mdDialog.cancel();
            vm.isSaving = false;
            $state.go('news-management', null, { reload: true });
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.clear = function clear() {
            $mdDialog.cancel();
        }

        // Create path news
        function to_slug(str) {
            // Chuyển hết sang chữ thường
            str = str.toLowerCase();

            // xóa dấu
            str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            str = str.replace(/(đ)/g, 'd');

            // Xóa ký tự đặc biệt
            str = str.replace(/([^0-9a-z-\s])/g, '');

            // Xóa khoảng trắng thay bằng ký tự -
            str = str.replace(/(\s+)/g, '-');

            // xóa phần dự - ở đầu
            str = str.replace(/^-+/g, '');

            // remove phần dư - ở cuối
            str = str.replace(/-+$/g, '');

            return str;
        }

        $scope.createLinkUpload = createLinkUpload;
        vm.isUpload = true;
        function createLinkUpload() {
            vm.isUpload = true;
            var formData = new FormData();
            formData.append('file', angular.element(document.querySelector('#uploadFileInput'))[0].files[0]);
            // PostFileAttach.upload(formData, onUploadSuccess, onUploadError);
            $http({
                method: 'POST',
                url: '/api/upload/upload-file',
                data: formData,
                //assign content-type as undefined, the browser
                //will assign the correct boundary for us
                headers: { 'Content-Type': undefined },
                //prevents serializing payload.  don't do it.
                transformRequest: angular.identity
                // 2 lines above help create link file upload
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                vm.contentPost.linkBanner = response.data.link;
                vm.isUpload = false;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response.status)
                vm.isUpload = false;
            });


        }

    }
})();
