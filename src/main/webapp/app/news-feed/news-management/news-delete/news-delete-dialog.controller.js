(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsDeleteController',NewsDeleteController);

    NewsDeleteController.$inject = ['$mdDialog', 'entity', 'News', '$state'];

    function NewsDeleteController($mdDialog, entity, News, $state) {
        var vm = this;

        vm.news = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $mdDialog.cancel();
        }

        function confirmDelete (id) {
            News.delete({id: id},
                function () {
                    $mdDialog.cancel();
                    $state.go('news-management', null, { reload: true });
                });
        }
    }
})();
