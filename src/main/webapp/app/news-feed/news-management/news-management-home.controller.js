(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsManagementHomeController', NewsManagementHomeController);

    NewsManagementHomeController.$inject = ['$scope', '$state', 'Principal', '$mdDialog', 'NewsCategory', 'GetListNewsByCate', 'News'];

    function NewsManagementHomeController($scope, $state, Principal, $mdDialog, NewsCategory, GetListNewsByCate, News) {
        var vm = this;

        vm.newscategories = NewsCategory.query();

        vm.popDialogCreateNews = popDialogCreateNews;

        function popDialogCreateNews() {
            $mdDialog.show({
                controller: 'NewsManagementHomeDialogController',
                controllerAs: 'vm',
                templateUrl: "app/news-feed/news-management/news-management-home-dialog.html",
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: true,
                resolve: {
                    entity: function() {
                        return {
                            "content": null,
                            "createdBy": null,
                            "createdDate": null,
                            "description": null,
                            "linkBanner": null,
                            "newsCategoryId": null,
                            "newsPath": null,
                            "newsTitle": null
                        };
                    }
                }
            }).then(function(result) {

            }, function() {

            });
        }

        vm.popDialogUpdateNews = popDialogUpdateNews;

        function popDialogUpdateNews(idNews) {
            $mdDialog.show({
                controller: 'NewsManagementHomeDialogController',
                controllerAs: 'vm',
                templateUrl: "app/news-feed/news-management/news-management-home-dialog.html",
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: true,
                resolve: {
                    entity: ['News', function(News) {
                        return News.get({ id: idNews });
                    }]
                }
            }).then(function(result) {

            }, function() {

            });
        }

        vm.popDialogDeleteNews = popDialogDeleteNews;

        function popDialogDeleteNews(idNews) {
            $mdDialog.show({
                controller: 'NewsDeleteController',
                controllerAs: 'vm',
                templateUrl: "app/news-feed/news-management/news-delete/news-delete-dialog.html",
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                fullscreen: true,
                resolve: {
                    entity: ['News', function(News) {
                        return News.get({ id: idNews });
                    }]
                }
            }).then(function(result) {

            }, function() {

            });
        }

        vm.selectedTab = selectedTab;

        function selectedTab(idTab) {
            GetListNewsByCate.query({ id: idTab }, function(res) {
                vm.listNewsByCate = [];
                vm.listNewsByCate = res[0].newsListInCate;
                // console.log(vm.listNewsByCate);
                // console.log(res);
            });
        }
    }
})();
