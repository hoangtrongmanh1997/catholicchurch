(function() {
    'use strict';
    angular
        .module('churchcatholicApp')
        .factory('GetListNewsByCate', GetListNewsByCate)
        .factory('News', News)
        .factory('PostFileAttach', PostFileAttach);

    GetListNewsByCate.$inject = ['$resource', 'DateUtils'];
    News.$inject = ['$resource', 'DateUtils'];
    PostFileAttach.$inject = ['$resource'];

    function GetListNewsByCate($resource, DateUtils) {
        var resourceUrl = 'api/news-categories/get-all-news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                isArray: true,
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            },
        });
    }

    function News($resource, DateUtils) {
        var resourceUrl = 'api/news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                // isObject: true,
                method: 'GET', 
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }

    function PostFileAttach($resource) {
        var resourceUrl = 'api/upload/upload-file';

        return $resource(resourceUrl, {}, {
            'upload': { method: 'POST' }
        });
    }

})();
