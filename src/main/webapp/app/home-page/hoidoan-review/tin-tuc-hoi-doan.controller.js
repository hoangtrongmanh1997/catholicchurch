(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsByHoiDoanController', NewsByHoiDoanController);

    NewsByHoiDoanController.$inject = ['$state', 'entity', 'GetDataCrawlHandleCommon'];

    function NewsByHoiDoanController($state, entity, GetDataCrawlHandleCommon) {
        var vm = this;

        vm.listNewsByHoiDoan = entity;
        // console.log(vm.listNewsByHoiDoan)
        vm.listData = GetDataCrawlHandleCommon.getListData();
        vm.listPostLctx = vm.listData.listPostLctx;
    }
})();
