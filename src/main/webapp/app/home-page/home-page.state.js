(function() {
    'use strict'

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('home-page', {
                parent: 'app',
                url: '/home-page',
                data: {
                    authorities: []
                },
                views: {
                    'navbar@': {
                        templateUrl: 'app/home-page/home-page.html',
                        controller: 'NavbarController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })

            .state('hoidoan-overview', {
                parent: 'app',
                url: '/tin-tuc-hoi-doan/{nameHoiDoan}-{id}',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/home-page/hoidoan-review/tin-tuc-hoi-doan.html',
                        controller: 'NewsByHoiDoanController',
                        controllerAs: 'vm'
                    }
                },
                // params: {
                //     id: null
                // },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'GetListNewsByCate', function($stateParams, GetListNewsByCate) {
                        return GetListNewsByCate.get({id : $stateParams.id}).$promise;
                    }]
                }
            })
    }

})();
