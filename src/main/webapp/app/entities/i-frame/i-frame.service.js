(function() {
    'use strict';
    angular
        .module('churchcatholicApp')
        .factory('IFrame', IFrame);

    IFrame.$inject = ['$resource', 'DateUtils'];

    function IFrame ($resource, DateUtils) {
        var resourceUrl =  'api/i-frames/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.updateDate = DateUtils.convertDateTimeFromServer(data.updateDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
