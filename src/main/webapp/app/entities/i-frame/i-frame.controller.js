(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('IFrameController', IFrameController);

    IFrameController.$inject = ['IFrame'];

    function IFrameController(IFrame) {

        var vm = this;

        vm.iFrames = [];

        loadAll();

        function loadAll() {
            IFrame.query(function(result) {
                vm.iFrames = result;
                vm.searchQuery = null;
            });
        }
    }
})();
