(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('i-frame', {
            parent: 'entity',
            url: '/i-frame',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'churchcatholicApp.iFrame.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/i-frame/i-frames.html',
                    controller: 'IFrameController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('iFrame');
                    $translatePartialLoader.addPart('cateIFrame');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('i-frame-detail', {
            parent: 'i-frame',
            url: '/i-frame/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'churchcatholicApp.iFrame.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/i-frame/i-frame-detail.html',
                    controller: 'IFrameDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('iFrame');
                    $translatePartialLoader.addPart('cateIFrame');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'IFrame', function($stateParams, IFrame) {
                    return IFrame.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'i-frame',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('i-frame-detail.edit', {
            parent: 'i-frame-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/i-frame/i-frame-dialog.html',
                    controller: 'IFrameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['IFrame', function(IFrame) {
                            return IFrame.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('i-frame.new', {
            parent: 'i-frame',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/i-frame/i-frame-dialog.html',
                    controller: 'IFrameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                linkIframe: null,
                                cateIframe: null,
                                updateDate: null,
                                updateBy: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('i-frame', null, { reload: 'i-frame' });
                }, function() {
                    $state.go('i-frame');
                });
            }]
        })
        .state('i-frame.edit', {
            parent: 'i-frame',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/i-frame/i-frame-dialog.html',
                    controller: 'IFrameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['IFrame', function(IFrame) {
                            return IFrame.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('i-frame', null, { reload: 'i-frame' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('i-frame.delete', {
            parent: 'i-frame',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/i-frame/i-frame-delete-dialog.html',
                    controller: 'IFrameDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['IFrame', function(IFrame) {
                            return IFrame.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('i-frame', null, { reload: 'i-frame' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
