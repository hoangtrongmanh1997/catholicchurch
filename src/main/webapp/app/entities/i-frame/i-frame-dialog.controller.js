(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('IFrameDialogController', IFrameDialogController);

    IFrameDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'IFrame'];

    function IFrameDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, IFrame) {
        var vm = this;

        vm.iFrame = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.iFrame.id !== null) {
                IFrame.update(vm.iFrame, onSaveSuccess, onSaveError);
            } else {
                IFrame.save(vm.iFrame, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('churchcatholicApp:iFrameUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.updateDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
