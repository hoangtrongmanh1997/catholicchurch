(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('IFrameDeleteController',IFrameDeleteController);

    IFrameDeleteController.$inject = ['$uibModalInstance', 'entity', 'IFrame'];

    function IFrameDeleteController($uibModalInstance, entity, IFrame) {
        var vm = this;

        vm.iFrame = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            IFrame.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
