(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('IFrameDetailController', IFrameDetailController);

    IFrameDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'IFrame'];

    function IFrameDetailController($scope, $rootScope, $stateParams, previousState, entity, IFrame) {
        var vm = this;

        vm.iFrame = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('churchcatholicApp:iFrameUpdate', function(event, result) {
            vm.iFrame = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
