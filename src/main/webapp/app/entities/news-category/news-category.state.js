(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-category', {
            parent: 'admin',
            url: '/news-category',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'churchcatholicApp.newsCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-category/news-categories.html',
                    controller: 'NewsCategoryController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('newsCategory');
                    $translatePartialLoader.addPart('type-cate');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('news-category-detail', {
            parent: 'news-category',
            url: '/news-category/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'churchcatholicApp.newsCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-category/news-category-detail.html',
                    controller: 'NewsCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('newsCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'NewsCategory', function($stateParams, NewsCategory) {
                    return NewsCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-category-detail.edit', {
            parent: 'news-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-category/news-category-dialog.html',
                    controller: 'NewsCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsCategory', function(NewsCategory) {
                            return NewsCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-category.new', {
            parent: 'news-category',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-category/news-category-dialog.html',
                    controller: 'NewsCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                categoryTitle: null,
                                categoryDesc: null,
                                createdDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-category', null, { reload: 'news-category' });
                }, function() {
                    $state.go('news-category');
                });
            }]
        })
        .state('news-category.edit', {
            parent: 'news-category',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-category/news-category-dialog.html',
                    controller: 'NewsCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsCategory', function(NewsCategory) {
                            return NewsCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-category', null, { reload: 'news-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-category.delete', {
            parent: 'news-category',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-category/news-category-delete-dialog.html',
                    controller: 'NewsCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsCategory', function(NewsCategory) {
                            return NewsCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-category', null, { reload: 'news-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
