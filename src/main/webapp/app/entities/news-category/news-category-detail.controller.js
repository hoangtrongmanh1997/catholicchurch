(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsCategoryDetailController', NewsCategoryDetailController);

    NewsCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsCategory', 'News'];

    function NewsCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsCategory, News) {
        var vm = this;

        vm.newsCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('churchcatholicApp:newsCategoryUpdate', function(event, result) {
            vm.newsCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
