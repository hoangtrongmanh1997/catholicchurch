(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsCategoryDialogController', NewsCategoryDialogController);

    NewsCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsCategory', 'News'];

    function NewsCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsCategory, News) {
        var vm = this;

        vm.newsCategory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsCategory.id !== null) {
                NewsCategory.update(vm.newsCategory, onSaveSuccess, onSaveError);
            } else {
                NewsCategory.save(vm.newsCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('churchcatholicApp:newsCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
