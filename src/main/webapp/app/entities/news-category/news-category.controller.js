(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsCategoryController', NewsCategoryController);

    NewsCategoryController.$inject = ['NewsCategory'];

    function NewsCategoryController(NewsCategory) {

        var vm = this;

        vm.newsCategories = [];

        loadAll();

        function loadAll() {
            NewsCategory.query(function(result) {
                vm.newsCategories = result;
                vm.searchQuery = null;
            });
        }
    }
})();
