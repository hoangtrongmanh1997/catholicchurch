(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('NewsCategoryDeleteController',NewsCategoryDeleteController);

    NewsCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsCategory'];

    function NewsCategoryDeleteController($uibModalInstance, entity, NewsCategory) {
        var vm = this;

        vm.newsCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
