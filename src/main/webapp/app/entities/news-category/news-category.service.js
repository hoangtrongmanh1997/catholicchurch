(function() {
    'use strict';
    angular
        .module('churchcatholicApp')
        .factory('NewsCategory', NewsCategory);

    NewsCategory.$inject = ['$resource', 'DateUtils'];

    function NewsCategory ($resource, DateUtils) {
        var resourceUrl =  'api/news-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
