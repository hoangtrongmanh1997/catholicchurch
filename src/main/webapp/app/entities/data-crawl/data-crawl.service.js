(function() {
    'use strict';
    angular
        .module('churchcatholicApp')
        .factory('DataCrawl', DataCrawl)
        .factory('DataCrawlPerDay', DataCrawlPerDay);

    DataCrawl.$inject = ['$resource'];
    DataCrawlPerDay.$inject = ['$resource'];

    function DataCrawl ($resource) {
        var resourceUrl =  'api/data-crawls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

    function DataCrawlPerDay ($resource) {
        var resourceUrl =  'api/data-crawls-per-day';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            }
        });
    }
})();
