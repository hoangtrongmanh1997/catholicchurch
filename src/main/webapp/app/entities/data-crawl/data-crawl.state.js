(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('data-crawl', {
            parent: 'entity',
            url: '/data-crawl',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'churchcatholicApp.dataCrawl.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-crawl/data-crawls.html',
                    controller: 'DataCrawlController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dataCrawl');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('data-crawl-detail', {
            parent: 'data-crawl',
            url: '/data-crawl/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'churchcatholicApp.dataCrawl.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-crawl/data-crawl-detail.html',
                    controller: 'DataCrawlDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dataCrawl');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DataCrawl', function($stateParams, DataCrawl) {
                    return DataCrawl.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'data-crawl',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('data-crawl-detail.edit', {
            parent: 'data-crawl-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-crawl/data-crawl-dialog.html',
                    controller: 'DataCrawlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataCrawl', function(DataCrawl) {
                            return DataCrawl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-crawl.new', {
            parent: 'data-crawl',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-crawl/data-crawl-dialog.html',
                    controller: 'DataCrawlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                linkImgBg: null,
                                titlePost: null,
                                descPost: null,
                                timePost: null,
                                linkPost: null,
                                contentPost: null,
                                source: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('data-crawl', null, { reload: 'data-crawl' });
                }, function() {
                    $state.go('data-crawl');
                });
            }]
        })
        .state('data-crawl.edit', {
            parent: 'data-crawl',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-crawl/data-crawl-dialog.html',
                    controller: 'DataCrawlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataCrawl', function(DataCrawl) {
                            return DataCrawl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-crawl', null, { reload: 'data-crawl' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-crawl.delete', {
            parent: 'data-crawl',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-crawl/data-crawl-delete-dialog.html',
                    controller: 'DataCrawlDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DataCrawl', function(DataCrawl) {
                            return DataCrawl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-crawl', null, { reload: 'data-crawl' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
