(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('DataCrawlController', DataCrawlController);

    DataCrawlController.$inject = ['DataCrawl'];

    function DataCrawlController(DataCrawl) {

        var vm = this;

        vm.dataCrawls = [];

        loadAll();

        function loadAll() {
            DataCrawl.query(function(result) {
                vm.dataCrawls = result;
                vm.searchQuery = null;
            });
        }
    }
})();
