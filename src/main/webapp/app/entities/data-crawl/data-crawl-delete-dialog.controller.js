(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('DataCrawlDeleteController',DataCrawlDeleteController);

    DataCrawlDeleteController.$inject = ['$uibModalInstance', 'entity', 'DataCrawl'];

    function DataCrawlDeleteController($uibModalInstance, entity, DataCrawl) {
        var vm = this;

        vm.dataCrawl = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DataCrawl.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
