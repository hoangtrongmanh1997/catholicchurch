(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('DataCrawlDetailController', DataCrawlDetailController);

    DataCrawlDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DataCrawl'];

    function DataCrawlDetailController($scope, $rootScope, $stateParams, previousState, entity, DataCrawl) {
        var vm = this;

        vm.dataCrawl = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('churchcatholicApp:dataCrawlUpdate', function(event, result) {
            vm.dataCrawl = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
