(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .controller('DataCrawlDialogController', DataCrawlDialogController);

    DataCrawlDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DataCrawl'];

    function DataCrawlDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DataCrawl) {
        var vm = this;

        vm.dataCrawl = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dataCrawl.id !== null) {
                DataCrawl.update(vm.dataCrawl, onSaveSuccess, onSaveError);
            } else {
                DataCrawl.save(vm.dataCrawl, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('churchcatholicApp:dataCrawlUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
