(function(){
	'use strict';

	angular.module('churchcatholicApp')
		.config(stateConfig);

	stateConfig.$inject = ['$stateProvider'];

	function stateConfig($stateProvider) {
		$stateProvider.state('dash-board-admin', {
			parent: 'admin',
			url: '/dash-board-admin',
			data: {
				authorities: []
			},
			views: {
				'content@': {
					controller: 'DashBoardContrl',
					controllerAs: 'vm',
					templateUrl: 'app/admin/dashboard/dashboard.html'
				}				
			}
		})
	}
})();