(function(){
	'use strict';

	angular.module('churchcatholicApp')
			.directive('myCarousel', myCarousel);

	myCarousel.$inject = ['$watch'];

	function myCarousel($watch) {
		return {
			link: function(scope, element, attrs) {
				scope.$watch(attrs.myCarousel, function(left, width){
					element.css({
						'position': 'absolute',
						'left': left,
						'width': width;	
					})
				})
			}
		};
	}
})();