(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .factory('GetDataCrawlHandleCommon', GetDataCrawlHandleCommon);

    GetDataCrawlHandleCommon.$inject = ['DataCrawlPerDay'];

    function GetDataCrawlHandleCommon(DataCrawlPerDay) {
        var vm = this;

        function getDataCrawl() {
            vm.listPostLctx = [];
            vm.listPostThaiHa = [];
            vm.listPostThaiHa2 = [];
            vm.listPostThaiHa3 = [];
            DataCrawlPerDay.query(function(res) {
                res.forEach(function(item) {
                    if (item.source == "LONG_CHUA_THUONG_XOT") {
                        vm.listPostLctx.push(item);
                    } else {
                        vm.listPostThaiHa.push(item);
                    }
                });
                for (var i = 0; i < 2; ++i) {
                    vm.listPostThaiHa2.push(vm.listPostThaiHa[i]);
                }
                for (var i = 2; i < 5; ++i) {
                    vm.listPostThaiHa3.push(vm.listPostThaiHa[i]);
                }

            });
        }
        getDataCrawl();
        return {
            getListData: function() {
                return vm;
            }
        };
    }
})();
