(function() {
    'use strict';

    angular
        .module('churchcatholicApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('hoidoan', {
                abstract: true,
                parent: 'app'
            })
            .state('giaoly', {
                abstract: true,
                parent: 'hoidoan'
            });           
    }
})();