package org.giaoxulaite.service;

import org.giaoxulaite.domain.DataCrawl;
import org.giaoxulaite.repository.DataCrawlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hoangmanh on 11/30/18.
 */
@Service
public class DataCrawlService {

    @Autowired
    private DataCrawlRepository dataCrawlRepository;

    public List<DataCrawl> getDataCrawlPerDay() {
        return dataCrawlRepository.getDataCrawlPerDay();
    }
}
