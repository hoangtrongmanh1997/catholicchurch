package org.giaoxulaite.service.impl;

import org.giaoxulaite.domain.News;
import org.giaoxulaite.service.NewsCategoryService;
import org.giaoxulaite.domain.NewsCategory;
import org.giaoxulaite.repository.NewsCategoryRepository;
import org.giaoxulaite.service.dto.NewsCategoryDTO;
import org.giaoxulaite.service.dto.NewsCustomDTO;
import org.giaoxulaite.service.mapper.NewsCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing NewsCategory.
 */
@Service
@Transactional
public class NewsCategoryServiceImpl implements NewsCategoryService {

    private final Logger log = LoggerFactory.getLogger(NewsCategoryServiceImpl.class);

    private final NewsCategoryRepository newsCategoryRepository;

    private final NewsCategoryMapper newsCategoryMapper;

    public NewsCategoryServiceImpl(NewsCategoryRepository newsCategoryRepository, NewsCategoryMapper newsCategoryMapper) {
        this.newsCategoryRepository = newsCategoryRepository;
        this.newsCategoryMapper = newsCategoryMapper;
    }

    /**
     * Save a newsCategory.
     *
     * @param newsCategoryDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public NewsCategoryDTO save(NewsCategoryDTO newsCategoryDTO) {
        log.debug("Request to save NewsCategory : {}", newsCategoryDTO);
        NewsCategory newsCategory = newsCategoryMapper.toEntity(newsCategoryDTO);
        newsCategory = newsCategoryRepository.save(newsCategory);
        return newsCategoryMapper.toDto(newsCategory);
    }

    /**
     * Get all the newsCategories.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<NewsCategoryDTO> findAll() {
        log.debug("Request to get all NewsCategories");
        return newsCategoryRepository.findAll().stream()
            .map(newsCategoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one newsCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public NewsCategoryDTO findOne(Long id) {
        log.debug("Request to get NewsCategory : {}", id);
        NewsCategory newsCategory = newsCategoryRepository.findOne(id);
        return newsCategoryMapper.toDto(newsCategory);
    }

    /**
     * Delete the newsCategory by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NewsCategory : {}", id);
        newsCategoryRepository.delete(id);
    }

    @Override
    public List<NewsCategoryDTO> getAllNewsInCate(Long idCate, Pageable pageable) {
        List<NewsCategoryDTO> newsCategoryDTOS = newsCategoryRepository.getAllNewsInCate(idCate);
        return newsCategoryDTOS.stream()
            .map(newsItem -> {
                NewsCategoryDTO newsCategoryDTO = new NewsCategoryDTO();
                newsCategoryDTO.setCategoryTitle(newsItem.getCategoryTitle());
                newsCategoryDTO.setId(newsItem.getId());
                newsCategoryDTO.setNewsListInCate(listSpecByPage( pageable, newsItem.getNewsListInCate()));
                return newsCategoryDTO;
            })
            .collect(Collectors.toList());
    }

    private List<NewsCustomDTO> listSpecByPage(Pageable pageable, List<NewsCustomDTO> newsListInCate) {
        // Do your process to get output in a list by using node.js run on a *js file defined in 'path' varriable
        Page<NewsCustomDTO> pages1 = new PageImpl<>(newsListInCate, new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()), newsListInCate.size());
        return pages1.getContent();
    }
}
