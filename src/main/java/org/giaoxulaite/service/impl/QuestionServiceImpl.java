package org.giaoxulaite.service.impl;

import org.giaoxulaite.domain.Answer;
import org.giaoxulaite.domain.enumeration.Level;
import org.giaoxulaite.repository.AnswerRepository;
import org.giaoxulaite.service.QuestionService;
import org.giaoxulaite.domain.Question;
import org.giaoxulaite.repository.QuestionRepository;
import org.giaoxulaite.service.dto.AnswerDTO;
import org.giaoxulaite.service.dto.QuestionDTO;
import org.giaoxulaite.service.mapper.QuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Service Implementation for managing Question.
 */
@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionServiceImpl.class);

    private final QuestionRepository questionRepository;

    private final QuestionMapper questionMapper;

    @Autowired
    private AnswerRepository answerRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository, QuestionMapper questionMapper) {
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
    }

    /**
     * Save a question.
     *
     * @param questionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public QuestionDTO save(QuestionDTO questionDTO) {
        log.debug("Request to save Question : {}", questionDTO);
        Question question = questionMapper.toEntity(questionDTO);
        question = questionRepository.save(question);
        return questionMapper.toDto(question);
    }

    /**
     * Get all the questions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<QuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Questions");
        return questionRepository.findAll(pageable)
            .map(questionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Question> findAll() {
        log.debug("Request to get all Questions Official");
        return questionRepository.findAll();
    }

    /**
     * Get one question by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public QuestionDTO findOne(Long id) {
        log.debug("Request to get Question : {}", id);
        Question question = questionRepository.findOne(id);
        return questionMapper.toDto(question);
    }

    /**
     * Delete the question by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Question : {}", id);
        questionRepository.delete(id);
    }

    public List<Long> getListQuestionLevel(Level level) {

        List<Long> listIdQuestion = questionRepository.listIdQuestion(level);
        int itemsSelect = 4; // items to select
        List<Long> selectedListQuestion = new ArrayList<Long>();
        int needed = itemsSelect;
        int available = listIdQuestion.size();
        Random rand = new Random();
        while (selectedListQuestion.size() < itemsSelect) {
            double probability = (double) needed / available;
//            System.out.println(probability);
            double randTmp = rand.nextDouble();
//            System.out.println(randTmp);
            if (rand.nextDouble() < (double) needed / available) {
                selectedListQuestion.add(listIdQuestion.get(available - 1));
                needed--;
            }
            available--;
        }
        return selectedListQuestion;

    }

    public List<Long> getListFullIdQuestion() {
        List<Long> listFullIdQuestion = new ArrayList<>();
        listFullIdQuestion.addAll(getListQuestionLevel(Level.EASY));
        listFullIdQuestion.addAll(getListQuestionLevel(Level.MEDIUM));
        listFullIdQuestion.addAll(getListQuestionLevel(Level.HARD));
        listFullIdQuestion.addAll(getListQuestionLevel(Level.DIFFICULT));
        return listFullIdQuestion;
    }

    @Override
    public List<QuestionDTO> getListFullQuestion(Pageable pageable) {
        Page<QuestionDTO> questionList = questionRepository.getListQuestion(getListFullIdQuestion(), pageable);
        System.out.println(questionList);
        for(QuestionDTO itemQuestion : questionList.getContent()) {
            itemQuestion.setPageable(pageable.getPageNumber());
        }
        return questionList.getContent();
    }

//  Fake data question - answer
    @Transactional
    @Override
    public void addNewListQuestions(List<Question> listProducts) {
        questionRepository.save(listProducts);
    }

    @Override
    public Question getOne(long id) {
        return questionRepository.findOne(id);
    }
//    End fake question - answer

    @Override
    public Page<QuestionDTO> getAllQuestion(Pageable pageable) {
        return questionRepository.getAllQuestion(pageable);
    }

    @Override
    public QuestionDTO getQuestionById(Long id) {
        return questionRepository.getQuestionById(id);
    }

    @Override
    public void deleteQuestion(Long id) {
        log.debug("Request to delete Question contain List Answer : {}", id);
        List<AnswerDTO> answerDTOList = answerRepository.getListAnswer(id);
        for(AnswerDTO itemAnswer: answerDTOList) {
            answerRepository.delete(itemAnswer.getId());
        }
        questionRepository.delete(id);

    }
}
