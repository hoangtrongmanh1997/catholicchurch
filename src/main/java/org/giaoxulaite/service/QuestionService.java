package org.giaoxulaite.service;

import org.giaoxulaite.domain.Answer;
import org.giaoxulaite.domain.Question;
import org.giaoxulaite.service.dto.QuestionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Question.
 */
public interface QuestionService {

    /**
     * Save a question.
     *
     * @param questionDTO the entity to save
     * @return the persisted entity
     */
    QuestionDTO save(QuestionDTO questionDTO);

    /**
     * Get all the questions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<QuestionDTO> findAll(Pageable pageable);
    List<Question> findAll();

    /**
     * Get the "id" question.
     *
     * @param id the id of the entity
     * @return the entity
     */
    QuestionDTO findOne(Long id);

    /**
     * Delete the "id" question.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<QuestionDTO> getListFullQuestion(Pageable pageable);

//    Function user fake data question - answer
    void addNewListQuestions(List<Question> listQuestions);

    Question getOne(long id);
//    End function fake
    Page<QuestionDTO> getAllQuestion(Pageable pageable);

    QuestionDTO getQuestionById(Long id);

    // use delete question contain answer
    void deleteQuestion(Long id);
}
