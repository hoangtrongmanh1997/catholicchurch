package org.giaoxulaite.service.mapper;

import org.giaoxulaite.domain.*;
import org.giaoxulaite.service.dto.NewsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity News and its DTO NewsDTO.
 */
@Mapper(componentModel = "spring", uses = {NewsCategoryMapper.class})
public interface NewsMapper extends EntityMapper<NewsDTO, News> {

    @Mapping(source = "newsCategory.id", target = "newsCategoryId")
    NewsDTO toDto(News news);

    @Mapping(source = "newsCategoryId", target = "newsCategory")
    News toEntity(NewsDTO newsDTO);

    default News fromId(Long id) {
        if (id == null) {
            return null;
        }
        News news = new News();
        news.setId(id);
        return news;
    }
}
