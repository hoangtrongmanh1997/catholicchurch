package org.giaoxulaite.service.mapper;

import org.giaoxulaite.domain.*;
import org.giaoxulaite.service.dto.NewsCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NewsCategory and its DTO NewsCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NewsCategoryMapper extends EntityMapper<NewsCategoryDTO, NewsCategory> {


    @Mapping(target = "news", ignore = true)
    NewsCategory toEntity(NewsCategoryDTO newsCategoryDTO);

    default NewsCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        NewsCategory newsCategory = new NewsCategory();
        newsCategory.setId(id);
        return newsCategory;
    }
}
