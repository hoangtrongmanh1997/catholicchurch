package org.giaoxulaite.service.dto;

/**
 * Created by hoangmanh on 11/30/18.
 */
public class DataCrawlDTO {
    private Long id;

    private String linkImgBg;

    private String titlePost;

    private String descPost;

    private String timePost;

    private String linkPost;

    private String contentPost;

    private String source;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkImgBg() {
        return linkImgBg;
    }

    public void setLinkImgBg(String linkImgBg) {
        this.linkImgBg = linkImgBg;
    }

    public String getTitlePost() {
        return titlePost;
    }

    public void setTitlePost(String titlePost) {
        this.titlePost = titlePost;
    }

    public String getDescPost() {
        return descPost;
    }

    public void setDescPost(String descPost) {
        this.descPost = descPost;
    }

    public String getTimePost() {
        return timePost;
    }

    public void setTimePost(String timePost) {
        this.timePost = timePost;
    }

    public String getLinkPost() {
        return linkPost;
    }

    public void setLinkPost(String linkPost) {
        this.linkPost = linkPost;
    }

    public String getContentPost() {
        return contentPost;
    }

    public void setContentPost(String contentPost) {
        this.contentPost = contentPost;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
