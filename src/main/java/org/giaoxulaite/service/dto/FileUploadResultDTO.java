package org.giaoxulaite.service.dto;

/**
 * Created by hoangmanh on 10/31/18.
 */
public class FileUploadResultDTO extends BaseApiResultDTO{
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
