package org.giaoxulaite.service.dto;


import org.giaoxulaite.domain.News;
import org.giaoxulaite.domain.NewsCategory;
import org.giaoxulaite.domain.enumeration.TypeCate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A DTO for the NewsCategory entity.
 */
public class NewsCategoryDTO implements Serializable {

    private Long id;

    private String categoryTitle;

    private String categoryDesc;

    private Instant createdDate;

    private TypeCate typeCate;

    private List<NewsCustomDTO> newsListInCate;

    @Autowired
    private ModelMapper modelMapper = new ModelMapper();

    public NewsCategoryDTO() {
    }

    public NewsCategoryDTO(NewsCategory newsCate) {
//        super();
        this.id = newsCate.getId();
        this.categoryTitle = newsCate.getCategoryTitle();
        this.newsListInCate = newsCate.getNews().stream()
                                    .map(post -> {
                                        NewsCustomDTO newsCustomDTO = new NewsCustomDTO();
                                        newsCustomDTO.setId(post.getId());
                                        newsCustomDTO.setNewsTitle(post.getNewsTitle());
                                        newsCustomDTO.setDescription(post.getDescription());
                                        newsCustomDTO.setCreatedDate(post.getCreatedDate());
                                        newsCustomDTO.setCreatedBy(post.getCreatedBy());
                                        newsCustomDTO.setNewsPath(post.getNewsPath());
                                        newsCustomDTO.setLinkBanner(post.getLinkBanner());
                                        newsCustomDTO.setNewsCategoryId(post.getNewsCategory().getId());
                                        return newsCustomDTO;
                                    }).collect(Collectors.toList());
    }

    public List<NewsCustomDTO> getNewsListInCate() {
        return newsListInCate;
    }

    public void setNewsListInCate(List<NewsCustomDTO> newsListInCate) {
        this.newsListInCate = newsListInCate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public TypeCate getTypeCate() {
        return typeCate;
    }

    public void setTypeCate(TypeCate typeCate) {
        this.typeCate = typeCate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NewsCategoryDTO newsCategoryDTO = (NewsCategoryDTO) o;
        if (newsCategoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsCategoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsCategoryDTO{" +
            "id=" + getId() +
            ", categoryTitle='" + getCategoryTitle() + "'" +
            ", categoryDesc='" + getCategoryDesc() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }

    private NewsCustomDTO convertToDto(News post) {
        NewsCustomDTO newsDTO = null;
        try {
            //statements that may cause an exception
            newsDTO = modelMapper.map(post, NewsCustomDTO.class);
            return newsDTO;
        } catch (Exception e) {
            System.out.println("ERROR!");
        }
        return newsDTO;
    }

    private List<NewsCustomDTO> reverseToNewsCustomDto(NewsCategory newsCate) {
        return new ArrayList<News>(newsCate.getNews()).stream()
            .map(post -> convertToDto(post))
            .collect(Collectors.toList());
    }
}
