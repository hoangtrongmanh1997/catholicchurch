package org.giaoxulaite.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.*;

import org.giaoxulaite.domain.Answer;
import org.giaoxulaite.domain.Question;
import org.giaoxulaite.domain.enumeration.Level;

/**
 * A DTO for the Question entity.
 */
public class QuestionDTO implements Serializable {

    private Long id;

    private String questionTitle;

    private String source;

    private Instant createdDate;

    private Level level;

    private int pageable;

    private List<Answer> answer;

    public QuestionDTO() {
    }

    public QuestionDTO(Question question) {
        super();
        this.questionTitle = question.getQuestionTitle();
        this.id = question.getId();
        this.source = question.getSource();
        this.level = question.getLevel();
        this.createdDate = question.getCreatedDate();
        this.answer = new ArrayList<Answer>(question.getAnswers());
    }

    public int getPageable() {
        return pageable;
    }

    public void setPageable(int pageable) {
        this.pageable = pageable;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuestionDTO questionDTO = (QuestionDTO) o;
        if(questionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), questionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + getId() +
            ", questionTitle='" + getQuestionTitle() + "'" +
            ", source='" + getSource() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", level='" + getLevel() + "'" +
            "}";
    }
}
