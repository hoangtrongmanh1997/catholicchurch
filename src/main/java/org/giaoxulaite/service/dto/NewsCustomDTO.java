package org.giaoxulaite.service.dto;

import java.io.Serializable;
import java.time.Instant;

/**
 * Created by hoangmanh on 10/28/18.
 */
public class NewsCustomDTO implements Serializable {
    private Long id;

    private String newsTitle;

    private String newsPath;

    private String linkBanner;

    private String description;

    private Instant createdDate;

    private String createdBy;

    private Long newsCategoryId;

    public NewsCustomDTO() {
    }

    public NewsCustomDTO(Long id, String newsTitle, String newsPath, String linkBanner, String description, Instant createdDate, String createdBy, Long newsCategoryId) {
        this.id = id;
        this.newsTitle = newsTitle;
        this.newsPath = newsPath;
        this.linkBanner = linkBanner;
        this.description = description;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.newsCategoryId = newsCategoryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsPath() {
        return newsPath;
    }

    public void setNewsPath(String newsPath) {
        this.newsPath = newsPath;
    }

    public String getLinkBanner() {
        return linkBanner;
    }

    public void setLinkBanner(String linkBanner) {
        this.linkBanner = linkBanner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(Long newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }
}
