package org.giaoxulaite.service.dto;

/**
 * Created by hoangmanh on 10/31/18.
 */
public class BaseApiResultDTO {
    private boolean isSuccess;
    private String message;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
