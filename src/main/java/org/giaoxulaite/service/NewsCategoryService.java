package org.giaoxulaite.service;

import org.giaoxulaite.service.dto.NewsCategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing NewsCategory.
 */
public interface NewsCategoryService {

    /**
     * Save a newsCategory.
     *
     * @param newsCategoryDTO the entity to save
     * @return the persisted entity
     */
    NewsCategoryDTO save(NewsCategoryDTO newsCategoryDTO);

    /**
     * Get all the newsCategories.
     *
     * @return the list of entities
     */
    List<NewsCategoryDTO> findAll();

    /**
     * Get the "id" newsCategory.
     *
     * @param id the id of the entity
     * @return the entity
     */
    NewsCategoryDTO findOne(Long id);

    /**
     * Delete the "id" newsCategory.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<NewsCategoryDTO> getAllNewsInCate(Long idCate, Pageable pageable);
}
