package org.giaoxulaite.service.schedule;

import org.giaoxulaite.domain.DataCrawl;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hoangmanh on 11/29/18.
 */
public class CrawlData {

    private HashSet<DataCrawl> links;

    private HashSet<DataCrawl> linksLctx;

    private List<List<String>> articles;

    public CrawlData() {
        links = new HashSet<DataCrawl>();
        linksLctx = new HashSet<>();
        articles = new ArrayList<List<String>>();
    }

    //Find all URLs that start with "http://www.mkyong.com/page/" and add them to the HashSet
    public HashSet<DataCrawl> getPageLinks(String URL) {
        try {
            Document document = Jsoup.connect(URL).get();
            Elements listPosts = document.select("#featured-posts .featured-post .featured-post-inner");

            for (Element post : listPosts) {
//                System.out.println(post);
                DataCrawl postItem = new DataCrawl();
                postItem.setTitlePost(post.select(".featured-title h2 > a").text());
                String attr = post.select(".featured-post-inner").attr("style");
                postItem.setLinkImgBg(attr.substring( attr.indexOf("http://"), attr.indexOf(")")));
                postItem.setDescPost(post.select(".featured-title h3").text());
                postItem.setTimePost(post.select(".featured-title .tie-date").text());
                postItem.setLinkPost(post.select("a[href]").attr("href"));
                System.out.println(postItem.toString());
                links.add(postItem);
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return links;
    }

    public HashSet<DataCrawl> getPageLCTX(String URL) {
        try {
            Document document = Jsoup.connect(URL).get();
            Elements listPosts = document.select(".news_column .featured");

            for (Element post : listPosts) {
                System.out.println(post);
                DataCrawl postItem = new DataCrawl();
                postItem.setTitlePost(post.select("a[href]").attr("title"));
                postItem.setLinkImgBg("https://tinthac.net" + post.select("img").attr("src"));
                postItem.setDescPost(post.ownText());
                postItem.setTimePost(post.select(".text-muted li").first().text());
                postItem.setLinkPost("https://tinthac.net" + post.select("a[href]").attr("href"));
                System.out.println(postItem.toString());
                linksLctx.add(postItem);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return linksLctx;
    }

    //    Connect to each link saved in the article and find all the articles in the page
    public void getArticles() {

        links.forEach(x -> {
            Document document;
            try {
                document = Jsoup.connect(x.getLinkPost()).get();
                Element articleLinks = document.select("#the-post .entry").first();
                System.out.println("-------------------------------------");
                System.out.println(articleLinks);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        });
    }
}
