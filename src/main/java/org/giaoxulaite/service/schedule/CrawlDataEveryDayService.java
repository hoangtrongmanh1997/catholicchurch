package org.giaoxulaite.service.schedule;

import com.codahale.metrics.annotation.Timed;
import org.giaoxulaite.domain.DataCrawl;
import org.giaoxulaite.repository.DataCrawlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hoangmanh on 11/29/18.
 */
@Service
public class CrawlDataEveryDayService {

    @Autowired
    private DataCrawlRepository dataCrawlRepository;

    @Scheduled(cron = "0 0 5 * * TUE-SAT" ) // Run at 5:00 AM on Tue to Sat
//	@Scheduled(fixedRate = 5000) // Run everyday at 8:00 AM
    @Timed
    public void crawlData() {
        CrawlData crawlData = new CrawlData();
        HashSet<DataCrawl> dataCrawl = crawlData.getPageLinks("http://nhathothaiha.net/");
        HashSet<DataCrawl> dataCrawlsLctx = crawlData.getPageLCTX("https://tinthac.net/index.php/nhat-ky-bai-giang/");
//        crawlData.getArticles();
        for (DataCrawl itemPost : dataCrawl) {
            itemPost.setSource("NHA_THO_THAI_HA");
            dataCrawlRepository.save(itemPost);
        }

        for (DataCrawl itemPost : dataCrawlsLctx) {
            itemPost.setSource("LONG_CHUA_THUONG_XOT");
            dataCrawlRepository.save(itemPost);
        }

    }
}
