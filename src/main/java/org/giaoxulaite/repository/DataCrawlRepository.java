package org.giaoxulaite.repository;

import org.giaoxulaite.domain.DataCrawl;
import org.giaoxulaite.service.dto.DataCrawlDTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the DataCrawl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataCrawlRepository extends JpaRepository<DataCrawl, Long> {
    @Query(value = "select * from data_crawl ORDER BY id DESC LIMIT 6", nativeQuery = true)
    List<DataCrawl> getDataCrawlPerDay();
}
