package org.giaoxulaite.repository;

import org.giaoxulaite.domain.Answer;
import org.giaoxulaite.service.dto.AnswerDTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Answer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    @Query("select new org.giaoxulaite.service.dto.AnswerDTO(ans) from Answer ans where ans.id in (?1)")
    List<AnswerDTO> getListStatusAnswered(List<Long> listId);

    @Query("select new org.giaoxulaite.service.dto.AnswerDTO(ans) from Answer ans where ans.questionId = (?1)")
    List<AnswerDTO> getListAnswer(Long questionId);

}

