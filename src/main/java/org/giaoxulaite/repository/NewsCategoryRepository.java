package org.giaoxulaite.repository;

import org.giaoxulaite.domain.NewsCategory;
import org.giaoxulaite.service.dto.NewsCategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the NewsCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsCategoryRepository extends JpaRepository<NewsCategory, Long> {
    @Query("select new org.giaoxulaite.service.dto.NewsCategoryDTO(newsCate) from NewsCategory newsCate where newsCate.id = ?1")
    List<NewsCategoryDTO> getAllNewsInCate(Long idCate);


}
