package org.giaoxulaite.repository;

import org.giaoxulaite.domain.Question;
import org.giaoxulaite.domain.enumeration.Level;
import org.giaoxulaite.service.dto.QuestionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    @Query("select q.id from Question q where q.level like ?1")
    List<Long> listIdQuestion(Level level);

    @Query("select new org.giaoxulaite.service.dto.QuestionDTO(q) from Question q where q.id in (?1)")
    Page<QuestionDTO> getListQuestion(List<Long> id, Pageable pageable);

    @Query(value = "select new org.giaoxulaite.service.dto.QuestionDTO(q) from Question q where id=?1")
    QuestionDTO getQuestionById(Long id);

    @Query(value = "select new org.giaoxulaite.service.dto.QuestionDTO(q) from Question q")
    Page<QuestionDTO> getAllQuestion(Pageable pageable);
}
