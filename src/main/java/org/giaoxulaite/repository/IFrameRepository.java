package org.giaoxulaite.repository;

import org.giaoxulaite.domain.IFrame;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the IFrame entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IFrameRepository extends JpaRepository<IFrame, Long> {

}
