package org.giaoxulaite.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.giaoxulaite.domain.DataCrawl;

import org.giaoxulaite.repository.DataCrawlRepository;
import org.giaoxulaite.service.DataCrawlService;
import org.giaoxulaite.service.dto.DataCrawlDTO;
import org.giaoxulaite.web.rest.errors.BadRequestAlertException;
import org.giaoxulaite.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DataCrawl.
 */
@RestController
@RequestMapping("/api")
public class DataCrawlResource {

    private final Logger log = LoggerFactory.getLogger(DataCrawlResource.class);

    private static final String ENTITY_NAME = "dataCrawl";

    private final DataCrawlRepository dataCrawlRepository;

    @Autowired
    private DataCrawlService dataCrawlService;

    public DataCrawlResource(DataCrawlRepository dataCrawlRepository) {
        this.dataCrawlRepository = dataCrawlRepository;
    }

    /**
     * POST  /data-crawls : Create a new dataCrawl.
     *
     * @param dataCrawl the dataCrawl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataCrawl, or with status 400 (Bad Request) if the dataCrawl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-crawls")
    @Timed
    public ResponseEntity<DataCrawl> createDataCrawl(@RequestBody DataCrawl dataCrawl) throws URISyntaxException {
        log.debug("REST request to save DataCrawl : {}", dataCrawl);
        if (dataCrawl.getId() != null) {
            throw new BadRequestAlertException("A new dataCrawl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DataCrawl result = dataCrawlRepository.save(dataCrawl);
        return ResponseEntity.created(new URI("/api/data-crawls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-crawls : Updates an existing dataCrawl.
     *
     * @param dataCrawl the dataCrawl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataCrawl,
     * or with status 400 (Bad Request) if the dataCrawl is not valid,
     * or with status 500 (Internal Server Error) if the dataCrawl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-crawls")
    @Timed
    public ResponseEntity<DataCrawl> updateDataCrawl(@RequestBody DataCrawl dataCrawl) throws URISyntaxException {
        log.debug("REST request to update DataCrawl : {}", dataCrawl);
        if (dataCrawl.getId() == null) {
            return createDataCrawl(dataCrawl);
        }
        DataCrawl result = dataCrawlRepository.save(dataCrawl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataCrawl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-crawls : get all the dataCrawls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dataCrawls in body
     */
    @GetMapping("/data-crawls")
    @Timed
    public List<DataCrawl> getAllDataCrawls() {
        log.debug("REST request to get all DataCrawls");
        return dataCrawlRepository.findAll();
    }

    /**
     * GET  /data-crawls/:id : get the "id" dataCrawl.
     *
     * @param id the id of the dataCrawl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataCrawl, or with status 404 (Not Found)
     */
    @GetMapping("/data-crawls/{id}")
    @Timed
    public ResponseEntity<DataCrawl> getDataCrawl(@PathVariable Long id) {
        log.debug("REST request to get DataCrawl : {}", id);
        DataCrawl dataCrawl = dataCrawlRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataCrawl));
    }

    /**
     * DELETE  /data-crawls/:id : delete the "id" dataCrawl.
     *
     * @param id the id of the dataCrawl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-crawls/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataCrawl(@PathVariable Long id) {
        log.debug("REST request to delete DataCrawl : {}", id);
        dataCrawlRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/data-crawls-per-day")
    @Timed
    public List<DataCrawl> getAllDataCrawlsPerDay() {
        log.debug("REST request to get all DataCrawls per Day");
        return dataCrawlService.getDataCrawlPerDay();
    }
}
