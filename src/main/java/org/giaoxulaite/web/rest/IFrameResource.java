package org.giaoxulaite.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.giaoxulaite.domain.IFrame;

import org.giaoxulaite.repository.IFrameRepository;
import org.giaoxulaite.web.rest.errors.BadRequestAlertException;
import org.giaoxulaite.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing IFrame.
 */
@RestController
@RequestMapping("/api")
public class IFrameResource {

    private final Logger log = LoggerFactory.getLogger(IFrameResource.class);

    private static final String ENTITY_NAME = "iFrame";

    private final IFrameRepository iFrameRepository;

    public IFrameResource(IFrameRepository iFrameRepository) {
        this.iFrameRepository = iFrameRepository;
    }

    /**
     * POST  /i-frames : Create a new iFrame.
     *
     * @param iFrame the iFrame to create
     * @return the ResponseEntity with status 201 (Created) and with body the new iFrame, or with status 400 (Bad Request) if the iFrame has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/i-frames")
    @Timed
    public ResponseEntity<IFrame> createIFrame(@RequestBody IFrame iFrame) throws URISyntaxException {
        log.debug("REST request to save IFrame : {}", iFrame);
        if (iFrame.getId() != null) {
            throw new BadRequestAlertException("A new iFrame cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IFrame result = iFrameRepository.save(iFrame);
        return ResponseEntity.created(new URI("/api/i-frames/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /i-frames : Updates an existing iFrame.
     *
     * @param iFrame the iFrame to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated iFrame,
     * or with status 400 (Bad Request) if the iFrame is not valid,
     * or with status 500 (Internal Server Error) if the iFrame couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/i-frames")
    @Timed
    public ResponseEntity<IFrame> updateIFrame(@RequestBody IFrame iFrame) throws URISyntaxException {
        log.debug("REST request to update IFrame : {}", iFrame);
        if (iFrame.getId() == null) {
            return createIFrame(iFrame);
        }
        IFrame result = iFrameRepository.save(iFrame);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, iFrame.getId().toString()))
            .body(result);
    }

    /**
     * GET  /i-frames : get all the iFrames.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of iFrames in body
     */
    @GetMapping("/i-frames")
    @Timed
    public List<IFrame> getAllIFrames() {
        log.debug("REST request to get all IFrames");
        return iFrameRepository.findAll();
        }

    /**
     * GET  /i-frames/:id : get the "id" iFrame.
     *
     * @param id the id of the iFrame to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the iFrame, or with status 404 (Not Found)
     */
    @GetMapping("/i-frames/{id}")
    @Timed
    public ResponseEntity<IFrame> getIFrame(@PathVariable Long id) {
        log.debug("REST request to get IFrame : {}", id);
        IFrame iFrame = iFrameRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(iFrame));
    }

    /**
     * DELETE  /i-frames/:id : delete the "id" iFrame.
     *
     * @param id the id of the iFrame to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/i-frames/{id}")
    @Timed
    public ResponseEntity<Void> deleteIFrame(@PathVariable Long id) {
        log.debug("REST request to delete IFrame : {}", id);
        iFrameRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
