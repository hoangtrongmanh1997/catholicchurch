package org.giaoxulaite.web.rest;

import org.giaoxulaite.constant.Constant;
import org.giaoxulaite.service.FileStorageService;
import org.giaoxulaite.service.dto.FileUploadResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by hoangmanh on 10/31/18.
 */
@RestController
@RequestMapping("/api/upload")
public class UploadFileResource {
    @Autowired
    FileStorageService storageService;

    @PostMapping("/upload-file")
    public FileUploadResultDTO uploadImage(
        @RequestParam("file") MultipartFile file) {
        String message = "";
        FileUploadResultDTO result = new FileUploadResultDTO();
        try {
            String newFilename = storageService.store(file);
            message = "You successfully uploaded " +
                file.getOriginalFilename() + "!";
            result.setMessage(message);
            result.setSuccess(true);
            result.setLink(Constant.PREFIX_LINK_UPLOAD +
                newFilename);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
