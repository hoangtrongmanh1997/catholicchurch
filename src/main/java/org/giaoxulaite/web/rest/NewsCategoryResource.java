package org.giaoxulaite.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.giaoxulaite.service.NewsCategoryService;
import org.giaoxulaite.web.rest.errors.BadRequestAlertException;
import org.giaoxulaite.web.rest.util.HeaderUtil;
import org.giaoxulaite.service.dto.NewsCategoryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NewsCategory.
 */
@RestController
@RequestMapping("/api")
public class NewsCategoryResource {

    private final Logger log = LoggerFactory.getLogger(NewsCategoryResource.class);

    private static final String ENTITY_NAME = "newsCategory";

    private final NewsCategoryService newsCategoryService;

    public NewsCategoryResource(NewsCategoryService newsCategoryService) {
        this.newsCategoryService = newsCategoryService;
    }

    /**
     * POST  /news-categories : Create a new newsCategory.
     *
     * @param newsCategoryDTO the newsCategoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsCategoryDTO, or with status 400 (Bad Request) if the newsCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-categories")
    @Timed
    public ResponseEntity<NewsCategoryDTO> createNewsCategory(@RequestBody NewsCategoryDTO newsCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save NewsCategory : {}", newsCategoryDTO);
        if (newsCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new newsCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsCategoryDTO result = newsCategoryService.save(newsCategoryDTO);
        return ResponseEntity.created(new URI("/api/news-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-categories : Updates an existing newsCategory.
     *
     * @param newsCategoryDTO the newsCategoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsCategoryDTO,
     * or with status 400 (Bad Request) if the newsCategoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the newsCategoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-categories")
    @Timed
    public ResponseEntity<NewsCategoryDTO> updateNewsCategory(@RequestBody NewsCategoryDTO newsCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update NewsCategory : {}", newsCategoryDTO);
        if (newsCategoryDTO.getId() == null) {
            return createNewsCategory(newsCategoryDTO);
        }
        NewsCategoryDTO result = newsCategoryService.save(newsCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-categories : get all the newsCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of newsCategories in body
     */
    @GetMapping("/news-categories")
    @Timed
    public List<NewsCategoryDTO> getAllNewsCategories() {
        log.debug("REST request to get all NewsCategories");
        return newsCategoryService.findAll();
        }

    /**
     * GET  /news-categories/:id : get the "id" newsCategory.
     *
     * @param id the id of the newsCategoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsCategoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/news-categories/{id}")
    @Timed
    public ResponseEntity<NewsCategoryDTO> getNewsCategory(@PathVariable Long id) {
        log.debug("REST request to get NewsCategory : {}", id);
        NewsCategoryDTO newsCategoryDTO = newsCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsCategoryDTO));
    }

    /**
     * DELETE  /news-categories/:id : delete the "id" newsCategory.
     *
     * @param id the id of the newsCategoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsCategory(@PathVariable Long id) {
        log.debug("REST request to delete NewsCategory : {}", id);
        newsCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/news-categories/get-all-news/{id}")
    @Timed
    public List<NewsCategoryDTO> getAllNewsByCategory(@PathVariable Long id, Pageable pageable) {
        return newsCategoryService.getAllNewsInCate(id, pageable);
    }
}
