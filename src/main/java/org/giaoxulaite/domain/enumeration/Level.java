package org.giaoxulaite.domain.enumeration;

/**
 * The Level enumeration.
 */
public enum Level {
    DIFFICULT, HARD, MEDIUM, EASY
}
