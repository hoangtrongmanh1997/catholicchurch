package org.giaoxulaite.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DataCrawl.
 */
@Entity
@Table(name = "data_crawl")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataCrawl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "link_img_bg")
    private String linkImgBg;

    @Column(name = "title_post")
    private String titlePost;

    @Column(name = "desc_post")
    private String descPost;

    @Column(name = "time_post")
    private String timePost;

    @Column(name = "link_post")
    private String linkPost;

    @Column(name = "content_post")
    private String contentPost;

    @Column(name = "source")
    private String source;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkImgBg() {
        return linkImgBg;
    }

    public DataCrawl linkImgBg(String linkImgBg) {
        this.linkImgBg = linkImgBg;
        return this;
    }

    public void setLinkImgBg(String linkImgBg) {
        this.linkImgBg = linkImgBg;
    }

    public String getTitlePost() {
        return titlePost;
    }

    public DataCrawl titlePost(String titlePost) {
        this.titlePost = titlePost;
        return this;
    }

    public void setTitlePost(String titlePost) {
        this.titlePost = titlePost;
    }

    public String getDescPost() {
        return descPost;
    }

    public DataCrawl descPost(String descPost) {
        this.descPost = descPost;
        return this;
    }

    public void setDescPost(String descPost) {
        this.descPost = descPost;
    }

    public String getTimePost() {
        return timePost;
    }

    public DataCrawl timePost(String timePost) {
        this.timePost = timePost;
        return this;
    }

    public void setTimePost(String timePost) {
        this.timePost = timePost;
    }

    public String getLinkPost() {
        return linkPost;
    }

    public DataCrawl linkPost(String linkPost) {
        this.linkPost = linkPost;
        return this;
    }

    public void setLinkPost(String linkPost) {
        this.linkPost = linkPost;
    }

    public String getContentPost() {
        return contentPost;
    }

    public DataCrawl contentPost(String contentPost) {
        this.contentPost = contentPost;
        return this;
    }

    public void setContentPost(String contentPost) {
        this.contentPost = contentPost;
    }

    public String getSource() {
        return source;
    }

    public DataCrawl source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataCrawl dataCrawl = (DataCrawl) o;
        if (dataCrawl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataCrawl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataCrawl{" +
            "id=" + getId() +
            ", linkImgBg='" + getLinkImgBg() + "'" +
            ", titlePost='" + getTitlePost() + "'" +
            ", descPost='" + getDescPost() + "'" +
            ", timePost='" + getTimePost() + "'" +
            ", linkPost='" + getLinkPost() + "'" +
            ", contentPost='" + getContentPost() + "'" +
            ", source='" + getSource() + "'" +
            "}";
    }
}
