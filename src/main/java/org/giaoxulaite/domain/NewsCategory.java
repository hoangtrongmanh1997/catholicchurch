package org.giaoxulaite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.giaoxulaite.domain.enumeration.Level;
import org.giaoxulaite.domain.enumeration.TypeCate;
import org.giaoxulaite.service.dto.NewsDTO;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A NewsCategory.
 */
@Entity
@Table(name = "news_category")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NewsCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "category_title")
    private String categoryTitle;

    @Column(name = "category_desc")
    private String categoryDesc;

    @Column(name = "created_date")
    private Instant createdDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_cate")
    private TypeCate typeCate;

    /**
     * A relationship
     */
    @ApiModelProperty(value = "A relationship")
    @OneToMany(mappedBy = "newsCategory")
    @JsonIgnore
//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<News> news = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public NewsCategory categoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
        return this;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public NewsCategory categoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
        return this;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public NewsCategory createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public TypeCate getTypeCate() {
        return typeCate;
    }

    public NewsCategory typeCate(TypeCate typeCate) {
        this.typeCate = typeCate;
        return this;
    }

    public void setTypeCate(TypeCate typeCate) {
        this.typeCate = typeCate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Set<News> getNews() {
        return news;
    }

    public NewsCategory news(Set<News> news) {
        this.news = news;
        return this;
    }

    public NewsCategory addNews(News news) {
        this.news.add(news);
        news.setNewsCategory(this);
        return this;
    }

    public NewsCategory removeNews(News news) {
        this.news.remove(news);
        news.setNewsCategory(null);
        return this;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsCategory newsCategory = (NewsCategory) o;
        if (newsCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsCategory{" +
            "id=" + getId() +
            ", categoryTitle='" + getCategoryTitle() + "'" +
            ", categoryDesc='" + getCategoryDesc() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
