package org.giaoxulaite.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import org.giaoxulaite.domain.enumeration.CateIFrame;

/**
 * A IFrame.
 */
@Entity
@Table(name = "i_frame")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class IFrame implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "link_iframe")
    private String linkIframe;

    @Enumerated(EnumType.STRING)
    @Column(name = "cate_iframe")
    private CateIFrame cateIframe;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "update_by")
    private String updateBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkIframe() {
        return linkIframe;
    }

    public IFrame linkIframe(String linkIframe) {
        this.linkIframe = linkIframe;
        return this;
    }

    public void setLinkIframe(String linkIframe) {
        this.linkIframe = linkIframe;
    }

    public CateIFrame getCateIframe() {
        return cateIframe;
    }

    public IFrame cateIframe(CateIFrame cateIframe) {
        this.cateIframe = cateIframe;
        return this;
    }

    public void setCateIframe(CateIFrame cateIframe) {
        this.cateIframe = cateIframe;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public IFrame updateDate(Instant updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public IFrame updateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IFrame iFrame = (IFrame) o;
        if (iFrame.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), iFrame.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IFrame{" +
            "id=" + getId() +
            ", linkIframe='" + getLinkIframe() + "'" +
            ", cateIframe='" + getCateIframe() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}
